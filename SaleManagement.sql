﻿--Drop database if exists
USE master
IF EXISTS ( SELECT 1 FROM sys.databases WHERE name = 'SaleManagement' )
DROP DATABASE SaleManagement

--Create database
CREATE DATABASE SaleManagement
GO
USE SaleManagement

--Create tables
--Tables without foreign key(s)
CREATE TABLE ProductType
(
	Id INT IDENTITY(1, 1),
	ProductTypeName NVARCHAR(200) NOT NULL,
	CONSTRAINT PK_ProductTypeId PRIMARY KEY (Id)
)

CREATE TABLE Employee
(
	Id INT IDENTITY(1,1),
	FirstName NVARCHAR(50) NOT NULL,
	LastName NVARCHAR(50) NOT NULL,
	Birthdate DATE NOT NULL,
	Gender NVARCHAR(4) NOT NULL,
	Email VARCHAR(50) NOT NULL,
	_Password VARCHAR(255) NOT NULL,
	CONSTRAINT PK_EmployeeId PRIMARY KEY (Id)
)

CREATE TABLE Customer
(
	Id INT IDENTITY(1,1),
	FirstName NVARCHAR(50) NOT NULL,
	LastName NVARCHAR(50) NOT NULL,
	Birthdate DATE NOT NULL,
	Gender NVARCHAR(4) NOT NULL,
	Email VARCHAR(50) NOT NULL,
	_Password VARCHAR(255) NOT NULL,
	_Address NVARCHAR(255) NOT NULL,
	CONSTRAINT PK_CustomerId PRIMARY KEY (Id)
)

CREATE TABLE Combo
(
	Id INT IDENTITY(1,1),
	ComboName NVARCHAR(100) NOT NULL,
	_Image TEXT NULL,
	Total FLOAT NOT NULL,	--Tổng tiền
	CONSTRAINT PK_ComboId PRIMARY KEY (Id)
)

--Tables with foreign key(s)
CREATE TABLE Product
(
	Id INT IDENTITY(1,1),
	ProductName NVARCHAR(200) NOT NULL,
	_Description NTEXT NULL,
	_Image TEXT NULL,
	Stock INT NOT NULL,		--Số lượng tồn
	Price FLOAT NOT NULL,	
	ProductTypeId INT NOT NULL,
	CONSTRAINT PK_ProductId PRIMARY KEY (Id),
	CONSTRAINT FK_Product_ProductType FOREIGN KEY (ProductTypeId) REFERENCES dbo.ProductType(Id)
)

CREATE TABLE Combo_Product
(
	ComboId INT NOT NULL,
	ProductId INT NOT NULL,	
	Quantity INT NOT NULL,
	UnitPrice FLOAT NOT NULL,   --Đơn giá (có thể bé hơn giá gốc của sản phẩm)
	Amount FLOAT NOT NULL,		--Thành tiền
	CONSTRAINT PK_ComboId_ProductId PRIMARY KEY(ComboId,ProductId),
	CONSTRAINT FK_ComboProduct_Combo FOREIGN KEY (ComboId) REFERENCES dbo.Combo(Id),
	CONSTRAINT FK_ComboProduct_Product FOREIGN KEY (ProductId) REFERENCES dbo.Product(Id)
)

CREATE TABLE StockLog
(
	Id INT IDENTITY(1,1),
	ProductId INT NOT NULL,
	Stock INT NOT NULL,
	_TimeStamp DATE NOT NULL,
	CONSTRAINT PK_StockLogId PRIMARY KEY (Id),
	CONSTRAINT FK_StockLog_Product FOREIGN KEY (ProductId) REFERENCES dbo.Product(Id)
)

CREATE TABLE Invoice
(
	Id INT IDENTITY(1,1),
	CustomerId INT NOT NULL,
	C_Address NVARCHAR(255) NOT NULL, 
	_Date DATETIME NOT NULL,
	_Status NVARCHAR(50) NOT NULL, 
	Total FLOAT NOT NULL,	--Tổng tiền
	CONSTRAINT PK_InvoiceId PRIMARY KEY (Id),
	CONSTRAINT FK_Invoice_Customer FOREIGN KEY (CustomerId) REFERENCES dbo.Customer(Id), 	 
)

CREATE TABLE InvoiceDetail_Product
(
	InvoiceId INT NOT NULL,
	ProductId INT NOT NULL,
	Quantity INT NOT NULL,
	UnitPrice FLOAT NOT NULL,	--Đơn giá
	Amount FLOAT NOT NULL,		--Thành tiền
	CONSTRAINT PK_InvoiceId_ProductId PRIMARY KEY (InvoiceId, ProductId),
	CONSTRAINT FK_InvoiceDetailProduct_Invoice FOREIGN KEY (InvoiceId) REFERENCES dbo.Invoice(Id),
	CONSTRAINT FK_InvoiceDetailProduct_Product FOREIGN KEY (ProductId) REFERENCES dbo.Product(Id)
)

CREATE TABLE InvoiceDetail_Combo
(
	InvoiceId INT NOT NULL,
	ComboId INT NOT NULL,
	Quantity INT NOT NULL,
	UnitPrice FLOAT NOT NULL,	--Đơn giá (giá tương ứng với mỗi combo)
	Amount FLOAT NOT NULL,		--Thành tiền
	CONSTRAINT PK_InvoiceId_ComboId PRIMARY KEY (InvoiceId, ComboId),
	CONSTRAINT FK_InvoiceDetailCombo_Invoice FOREIGN KEY (InvoiceId) REFERENCES dbo.Invoice(Id),
	CONSTRAINT FK_InvoiceDetailCombo_Combo FOREIGN KEY (ComboId) REFERENCES dbo.Combo(Id)
)

--INSERT DATAS
--TABLE ProductType
INSERT INTO dbo.ProductType( ProductTypeName )
VALUES ( 'Laptop'),
	   ( N'Máy bộ' ),
	   ( N'Linh kiện' ),
	   ( N'Phụ kiện' )

--TABLE Employee
INSERT INTO dbo.Employee ( FirstName, LastName, Birthdate, Gender, Email, [_Password] )
VALUES ( N'Hoàng Minh', N'Triết', '1999-03-24', 'Nam', 'triethoang@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
	   ( N'Lê Việt', N'Hoàng', '1999-02-13', 'Nam', 'leviethoang@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
       ( N'Nguyễn Hữu', N'Đức', '1999-08-02', 'Nam', 'nguyenhuuduc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
	   ( N'Mạc Vĩ', N'Hào', '1999-02-20', 'Nam', 'mvhix9@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
	   ( N'Phạm Minh', N'Hiển', '1999-03-27', 'Nam', 'pmhien273@gmail.com', 'e10adc3949ba59abbe56e057f20f883e')
	   
--TABLE Customer
INSERT INTO Customer ( FirstName, LastName, Birthdate, Gender, Email, [_Password], [_Address] ) 
VALUES ( N'Mai Kim', N'Trí', '1992-04-16', N'Nữ', 'maikimtri@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', N'69/20 Quang Trung Q12, TP.HCM'),
	   ( N'Lê Văn', N'Đạt', '1995-06-20', 'Nam', 'levandat@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', N'23A Trần Quang Diệu Q7, TP.HCM'),
	   ( N'Trần Đức', N'Bo', '1996-08-12', 'Nam', 'tranducbo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', N'102 Bến Thành Q1, TP.HCM')

--TABLE Product
INSERT INTO dbo.Product( ProductName, [_Description], [_Image], Stock, Price, ProductTypeId )
VALUES ( 'Laptop Dell Inspiron 15 7501 X3MRY1', N'Laptop sử dụng công nghệ 7nm hiện đại, trang bị tối đa tới vi xử lí AMD Ryzen 5 4600H và card đồ họa Radeon™ RX5300M 3GB, đem tới cho chiếc laptop khi chơi game có hiệu năng ngang tầm máy desktop.Hãy tận hưởng trải nghiệm chơi game và giải trí đa phương tiện tuyệt hảo. Sử dụng RAM 8G DDR4 3200 cho tốc độ tải dữ liệu nhanh vượt trội. Hiệu năng được cải thiện khoảng 10% so với DDR4-2400. AMD FreeSync™ Premium giúp các game thủ hạng nặng chơi game cực mượt, không bị xé hình. Không còn phải đánh đổi yếu tố nào nữa, thoải mái chơi game với hiệu năng cao, giảm thiểu giật hình và độ trễ thấp.Nắp của chiếc laptop gaming này và phần bàn phím được làm bằng kim loại, kết hợp với thiết kế hiện đại, đem lại cảm hứng để bước vào chiến trường ảo.Với cụm tản nhiệt riêng cho CPU và GPU sử dụng tổng cộng 7 ống dẫn nhiệt, kết hợp hiệu quả để tản nhiệt và tối ưu hóa luồng gió trong một thân máy siêu gọn. Sức mạnh cực đỉnh để làm việc và giải trí di động.Laptop Dell mang tới trải nghiệm âm thanh tốt nhất là với các bản thu lossless với chất lượng như âm thanh gốc.Chiếc laptop MSI này còn có phần mềm Dragon Center độc quyền của MSI giúp bạn điều chỉnh và tùy biến chiếc laptop theo ý muốn của bản thân. Theo dõi tình trạng hoạt động, điều chỉnh và tối ưu một cách dễ dàng trên một giao diện thống nhất.Được phát triển nhờ quan hệ hợp tác với BlueStacks, phần mềm MSI APP Player đem lại trải nghiệm chơi game mobile mượt mà trên nền tảng PC. Nó cho phép game thủ tùy chỉnh nhiều tính năng, như đèn nền bàn phím, khả năng xử lí đồ họa và chạy đa nhiệm.Bravo 15 có một cổng LAN RJ45, hai cổng Type-C USB 3.2 Gen 1, hai cổng Type-A USB 3.2 Gen 1 và một cổng HDMI, giúp kết nối cực kì dễ dàng.', 'laptop_dell_inspiron_15_7501_x3mry1.jpg', 10, 29000000, 1 ),
	   ( 'Laptop Asus TUF Gaming FA506II AL016T', N'Laptop ASUS Expertbook P1410CJA EK355T mang phong cách thiết kế tối giản, lịch lãm với các đường nét được bo tròn nhẹ nhàng cùng màu đen sao. Khung máy được gia cố chắc chắn cùng công nghệ bảo vệ ổ cứng EAR, bảo vệ dữ liệu khỏi các tác động vật lý. Với màn hình 14 inch cùng khối lượng chỉ 1.442kg. Máy khá gọn nhẹ, khả năng di động cao thích hợp để bạn mang đi khắp mọi nơi.Laptop ASUS Expertbook P1410CJA EK355T sở hữu bộ vi xử lý chip Intel i5 đời 10 mới nhất cho khả năng xử lý mọi tác vụ văn phòng nhanh gọn, kể các tác vụ yêu cầu tính toán phức tạp. Bộ nhớ RAM 8GB đa nhiệm, cùng lúc xử lý nhiều công việc hay mở nhiều tab Chrome không giật, lag. Ổ cứng SSD 512GB có tốc độ đọc ghi siêu nhanh, mở máy, khởi động phần mềm không độ trễ.Bàn phím mang thiết kế công thái học, kết cấu liền khối vững chắc của máy cùng với hành trình phím 1,4mm mang đến cảm giác gõ phím hoàn toàn thoải mái. Đèn led phím hỗ trợ làm việc tốt trong điều kiện ánh sáng yếu. Touchpad rộng rãi, nhanh nhạy, với những tác vụ thông thường sẽ không cần đến sự hỗ trợ của chuột rời. ASUS Expertbook P1410CJA EK355T còn được trang bị đầy đủ các cổng kết nối cần thiết, tiện lợi cho việc chia sẻ thông tin, dữ liệu hay kết nối với các thiết bị ngoại vi như cổng HDMI, đầu đọc thẻ SD, USB 2.0, cổng USB type C 3.1 cho phép kết nối 2 chiều,...', 'laptop_asus_tuf_gaming_fa506ii_al016t.png', 10, 21900000, 1 ),
	   ( 'Laptop HP Envy 13-ba1031TU 2K0B7PA', N'Laptop HP ENVY 13-ba1031TU 2K0B7PA sở hữu một thiết kế trẻ trung, năng động chắc chắn sẽ mang đến cho bạn sự tự tin nhất khi cầm trên tay. Đây là chiếc laptop kết hợp hoàn hảo của một chiếc laptop văn phòng với thời trang hiện đại. Được thiết kế theo xu hướng chuẩn hiện đại tương tự như các dòng laptop ultrabook hiện nay với kích thước mỏng hơn và nhẹ hơn. Khung máy bền bỉ với tông màu vàng lịch lãm sẽ đem lại một phong cách chuyên nghiệp cho bạn.Được trang bị con chip Intel® Core™ i7-1165G7 thế hệ mới nhất với xung nhịp 4.70GHz bạn có thể xử lý các tác vụ hằng ngày, công việc văn phòng, xem phim, lướt web một cách đơn giản. Bên cạnh đó, máy tính có 16GB RAM và 1TB PCIe® NVMe™ M.2  dung lượng SSD để cung cấp cho người dùng trải nghiệm đa nhiệm mượt mà, lưu trữ dữ liệu thoải mái và tốc độ truyền tải nhanh chóng.Sở hữu màn hình laptop 13.3 inch FullHD, hiển thị hình ảnh rõ nét, sống động. Công nghệ màn hình IPS sẽ cho bạn thưởng thức những bức ảnh màu sắc tươi sáng và rực rỡ nhất mà không gây khó chịu cho mắt.HP đã nghiên cứu và trang bị bàn phím hiện đại cùng TouchPad rộng trên chiếc máy tính này nhằm giúp người dùng có trải nghiệm nhập dữ liệu tối ưu. Khoảng cách giữa các phím bấm hợp lý, độ nảy cao và hành trình phím sâu tạo cảm giác nhập liệu nhanh chóng và thoải mái.Bàn di chuột TouchPad với diện tích lớn, hỗ trợ bạn sử dụng cảm ứng đa điểm để cuộn văn bản, xoay hình ảnh và nhiều thao tác tiện lợi.Để đáp ứng nhu cầu liên kết với các thiết bị ngoại vi, được trang bị đa dạng các cổng I/O. Ở cạnh bên phải là jack nguồn, 2 USB 3.2 Type A và combo tai nghe/micro 3.5mm. Ở cạnh trái đầy đủ hơn với HDMI 2.1, USB 3.1 Type A, Ethernet, USB 3.1 Type C và đầu đọc thẻ SD.', 'laptop_hp_envy_13-ba1031tu_2k0b7pa.jpg', 0, 23500000, 1 ),
	   ( 'Laptop Asus D509DA EJ116T', N'ASUS ExpertBook P2451FA EK0229T là sản phẩm hoàn hảo để mang lại hiệu toàn diện để giúp bạn vượt qua một ngày làm việc. Máy tính xách tay siêu nhẹ kết hợp cùng bản lề phẳng 180 độ để bạn có thể đặt laptop trên bất kỹ mặt phẳng nào, cực kỳ lý tưởng để chia sẻ nội dung trên màn hình với người khác.ExpertBook P2 kết hợp các yếu tố kiến trúc vào thiết kế với lớp hoàn thiện Star Black bóng bẩy và các góc được tạo hình chính xác, mang đến vẻ ngoài thanh lịch và chuyên nghiệp hơn hẵn so với các mẫu laptop giá rẻ khác.Máy có kích thước siêu mỏng và nhẹ 1,5kg cho phép bạn có thể dễ dàng cho vào một chiếc cặp hoặc ba lô để bạn có thể làm việc ở bất cứ đâu.Một màn hình NanoEdge viền mỏng cung cấp không gian làm việc rộng hơn. Tỉ lệ màn hình so với khung máy cũng giúp phần khiến cho mẫu laptop 14inch này nhìn không quá khác so với các dòng máy tính xách tay 13inch hiện nay.Asus ExpertBook P2 được thiết kế cho các doanh nhân cần hiệu năng ổn định để thực hiện công việc hàng ngày. Bộ xử lý Intel Core i5 thế hệ mới nhất mang đến hiệu năng nhanh và nhạy mà bạn cần. Với thiết kế lưu trữ kép cung cấp không gian lưu trữ rộng rãi và truy cập dữ liệu nhanh hơn. Laptop Asus ExpertBook P2 cũng là lựa chọn thích hợp trong tương lai, với thiết kế dễ truy cập giúp dễ dàng nâng cấp các thành phần bên trong để phục vụ nhiều nhu cầu làm việc hơn nữa.Khi bạn làm việc ở những nơi công cộng, kết nối là rất quan trọng. ExpertBook P2 cung cấp một bộ cổng I/O toàn diện để truyền dữ liệu dễ dàng và kết nối ngoại vi linh hoạt. Để đảm bảo độ tin cậy, các cổng được kiểm tra để chịu đựng tới 15.000 lần.Thiết kế khung gầm kết hợp các tính năng cung cấp thêm độ cứng cho kết cấu để đối phó với các trường hợp không may có thể diễn ra bất cứ lúc nào.Độ bền cuối cùng là giá trị cốt lõi mà ExpertBook P2 mang lại cho người dùng doanh nghiệp, đảm bảo tài sản có giá trị của bạn sẽ tồn tại bất cứ điều gì nghiêm ngặt hàng ngày, tác động ngẫu nhiên mà họ gặp phải. ExpertBook P2 được xây dựng chắc chắn và được kiểm tra chất lượng nghiêm ngặt theo tiêu chuẩn quân sự MIL-STD 810G của Hoa Kỳ với các bài kiểm tra chất lượng của ASUS.ExpertBook P2 bao gồm các tính năng bảo mật toàn diện cho doanh nghiệp để bảo vệ dữ liệu quan trọng của bạn. Một khe khóa Kensington bảo vệ chống trộm, trong khi tấm chắn bảo mật webcam, cảm biến vân tay và chip TPM 2.0 giữ cho những gì bên trong an toàn. Nếu bạn đang băn khoăn lo lắng về vấn đề bảo mật thì hãy chọn ngay cho mình sản phẩm laptop Asus này nhé!', 'laptop_asus_d509da_ej116t.jpeg', 8, 9350000, 1 ),
	   ( N'Máy bộ Workstation GVN G-Station K303', N'Được tối ưu cấu hình hoàn hảo cho trải nghiệm sử dụng bộ phần mềm Adobe ở cấp độ nhân viên/chuyên viên. Bộ máy này phục vụ tốt cho những công việc: Diễn họa viên, chuyên viên thiết kế đồ họa sử dụng các phần mềm Adobe Photoshop, Adobe Lightroom, Adobe Illustrator, … Sinh viên chuyên ngành thiết kế sử dụng các phần mềm thiết kế đồ họa 2D, 3D modeling: Adobe Dimension, Substance, ... Phóng viên, thợ chụp ảnh nghiệp dư, bán chuyên sử dụng các phần mềm Adobe Lightroom, Adobe Photoshop, … Biên tập viên, Editor Video sử dụng các phần mềm Adobe Premiere, Adobe After Effects, …Sử dụng nền tảng Vi xử lý từ Intel với độ ổn định cực kì cao và khả năng nâng cấp mạnh mẽ.', 'g-station_k303.jpg', 5, 37900000, 2 ),
	   ( N'Máy Workstation GVN G-Creator C502', N'Về các tính năng chính,Mainboard ASUS Prime X570-P có hai khe cắm PCIe 4.0 có độ dài đầy đủ hoạt động ở x16 và x16 / x4. Điều này là do bộ xử lý sê-ri Ryzen 3000 cung cấp khe cắm trên cùng, trong khi các làn khe dài đầy đủ phía dưới đến trực tiếp từ chipset X570. Hỗ trợ cho cấu hình card đồ họa đa chiều AMD CrossFire hai chiều, nhưng không hỗ trợ cho NVIDIA SLI. Đối với các thiết bị lưu trữ, có hai khe M.2 hỗ trợ các ổ PCIe 4.0 x4, nhưng người dùng muốn chạy các ổ NVMe chạy nhanh và nóng có thể cần phải mua tản nhiệt riêng vì bo mạch không có tính năng bao gồm trên Prime X570-P có tổng cộng sáu cổng SATA. Hỗ trợ bộ nhớ rất tốt với bốn khe cắm bộ nhớ với hỗ trợ lên tới 128 GB với UDIMM 32 GB đã được ASUS đủ điều kiện trong ngăn xếp sản phẩm X570 của mình. CPU AMD Ryzen 9 3900X đánh đúng tâm lý người dùng với một CPU đa nhân giá cả hợp lý đủ mạnh mẽ để phục vụ công việc và dĩ nhiên là thừa sức chiến tất cả các tựa game. Không chỉ đơn thuần là một chiếc CPU nhiều nhân, CPU AMD Ryzen 9 3900X còn sử dụng kiến trúc Zen 2 mới giúp cải thiện hiệu năng của từng nhân, kết hợp với tiến trình sản xuất 7 nm để tối ưu điện năng tiêu thụ. Chính vì vậy mà ngay cả khi tích hợp đến 12 nhân, điện năng tiêu thụ của CPU AMD Ryzen 9 3900X vẫn trong tầm kiểm soát và gần như cao hơn không đáng kể so với Ryzen 2700X 8 nhân/16 luồng trước đó. G.SKILL Trident Z RGB DDR4 Bus 3000 vẫn giữ nguyên thiết kế mang tính biểu tượng của dòng sản phẩm Trident Z truyền thống - có bộ tản nhiệt bằng nhôm sang trọng giúp tản nhiệt cực tốt. Được trang bị dải led RGB 16 triệu màu, hỗ trợ tùy chỉnh dễ dàng bằng phần mềm. Bên cạnh đó, còn được trang bị các chip nhớ IC đã được sàng lọc đặc biệt thông qua quá trình lựa chọn tuyệt vời của G.SKILL và một PCB mười lớp được thiết kế tùy chỉnh cung cấp độ ổn định tín hiệu tối đa, tương thích và hiệu suất trên rất nhiều các bo mạch chủ.', 'g-creator_c502.jpg', 3, 40390000, 2 ),
	   ( N'Máy Workstation GVN G-Creator C702', N'CPU Intel Core i9 10900K là bộ vi xử lý hàng đầu trong dòng sản phẩm Intel Comet Lake thế hệ thứ 10 và sản phẩm này cũng đánh dấu lần đầu tiên Intel đã đưa 10 lõi xử lý vào một bộ xử lý chính. i9 10900K sở hữu 10 nhân 20 luồng với tốc độ xử lý cơ bản 3.7GHz và tối đa có thể lên đến 5.3GHz kết hợp cùng công nghệ Heat Velocity Boost hiện đại. Nếu bạn có nhu cầu tìm làm việc với các tác vụ đồ họa cao cấp hay cần tìm cho mình một bộ PC có khả năng dựng - render 4K một cách mượt mà thì với i9 10900K, bạn có thể yên tâm tuyệt đối với hiệu năng mà CPU mang lại. Asus vừa cho ra mắt series bo mạch chủ Z490 cao cấp. Mainboard ASUS ROG Z490 MAXIMUS XII EXTREME sử dụng mô-đun cấp điện 16 pha có khả năng xử lý lên đến 90 ampe. Công nghệ ép xung thông minh - ProCool II giúp cải thiện đáng kể khả năng ép xung, nhất là khi kết hợp cùng bộ vi xử lý cao cấp i9 10900K đến từ Intel.  Để tăng hiệu quả tản nhiệt, thiết kế ống dẫn nhiệt chữ U có thể làm giảm đáng kể nhiệt độ khi CPU hoạt động ở công suất cao. Khu vực làm mát bằng nước của Z490 MAXIMUS XII EXTREME cho phép người dùng có thể theo dõi tình trạng hoạt động của bo mạch chủ và có khả năng tự điều chỉnh để tránh gặp những sự cố xảy ra khi lắp đặt hoặc phải hoạt động liên tục trong thời gian dài. Để tối ưu khả năng xử lý đa nhiệm, GEARVN cũng trang bị trên dòng PC cao cấp nhất 32GB với bus 3000. Ngoài hỗ trợ làm việc với các tác vụ đa nhiệm, bề ngoài bắt mắt của G.SKILL Trident Z RGB cũng là một điểm cộng lớn giúp góc gaming của bạn càng nổi bật hơn rất nhiều.Card đồ họa MSI GeForce RTX 3090 GAMING X TRIO 24G chắc chắn là sự lựa chọn tốt nhất hiện nay cho fan ROG nếu riêng và game thủ nói chung. Hiệu năng mạnh mẽ của RTX 3090 hoàn toàn có thể đáp ứng mọi yêu cầu mà những tựa game đình đám có đồ họa thuộc hàng khủng nhất hiện nay như:Call of Duty: Modern Warfare, Red Dead Redemption 2,...', 'g-creator_c702.jpg', 3, 7100000, 2 ),
	   ( 'Ram PC (8GB DDR4 1x8G 2666) RAM Kingston HyperX Fury Black', N'Ram Desktop Kingston HyperX Fury RGB là dòng RAM lâu năm của Kingston với chất lượng đã được khẳng định. Phiên bản DDR4 thích hợp cho cả hệ thống Intel và AMD đi cùng tốc độ rất cao, đạt 3200Mhz và nhiều tính năng hấp dẫn. Tạo phong cách đặc sắc cho dàn máy tính PC của bạn với bộ tản nhiệt mới cho FURY DDR4 RGB và các hiệu ứng chiếu sáng mượt mà, và ấn tượng. Dải LED RGB có các hiệu ứng cực kỳ mượt mà, tuyệt đẹp và có thể tùy chỉnh theo ý thích. Bạn có thể tùy chỉnh hiệu ứng RGB bằng phần mềm HyperX NGENUITY đi kèm cực kỳ mạnh mẽ hoặc có thể đồng bộ cực kỳ dễ dàng bằng phần mềm của Mainboard hỗ trợ. Ram HyperX Fury với các hiệu ứng RGB sẽ luôn được đồng bộ nhờ công nghệ Infrared Sync đã được cấp bằng sáng chế của HyperX. Trang bị công nghệ HyperX Infrared Sync  giúp hệ thống của bạn đồng bộ hiệu ứng cực kỳ dễ dàng mà không cần phải cắm dây tín hiệu RGB. Quá trình tự động ép xung lên đến tần số cao nhất mà Mainboard hỗ trợ. Chỉ cần cắm và chạy, không cần tinh chỉnh trong Bios. RAM Kingston HyperX Fury RGB (8GB DDR4 1x8G 3200) trang bị hệ thống tản nhiệt bằng nhôm màu đen, vừa giúp hệ thống máy tính của bạn trông hầm hố hơn vừa có tác dụng giải nhiệt cực tốt, giúp hệ thống của bạn chạy mát mẻ hơn.', 'ram_kingston_hyperx_fury_black.jpg', 20, 790000, 3 ),
	   ( N'Ổ cứng HDD Seagate Barracuda 1TB 7200rpm', NULL, 'hdd_seagate_barracuda_1tb_7200rpm.jpg', 12, 980000, 3 ),
	   ( 'Case XIGMATEK GEMINI White (Mini Tower)', N'Xigmatek Gemini, vỏ hộp LED Micro ATX PC cầu vồng đa chế độ được lập trình động mới, bảng điều khiển phông chữ được thiết kế với kiểu dáng bàn chải tóc cao cấp và bên trái là kính cường lực siêu rõ, hoàn hảo cho màn hình xây dựng hệ thống, cấu trúc khung và vỏ khung hoàn toàn đặc trưng Quản lý thân thiện, nó sẽ hỗ trợ bo mạch chủ M-ATX và Mini ITX với khe hở làm mát CPU ở 160mm và chiều dài tối đa của thẻ VGA ở 320mm.', 'case_xigmatek_gemini_white_(mini_tower).jpg', 0, 650000, 3 ),
	   ( N'Tản khí DEEPCOOL GAMMAXX 400 giá rẻ', N'Trở thành siêu sao Deepcool không hề đơn giản. GAMMAXX 400 đã nổi lên như một trong những bộ làm mát CPU được khuyên dùng nhiều nhất vì hiệu suất tản nhiệt tuyệt vời và hiệu ứng ánh sáng LED đỏ tuyệt vời. GAMMAXX 400 có thể được gắn với quạt thứ hai và là giải pháp nhiệt bắt buộc phải có trong môi trường chơi game.', 'deepcool_gammaxx_400.jpg', 14, 440000, 3 ),
	   ( N'Bàn phím Razer Blackwidow V3 Pro Green Switch', N'Bàn phím cơ Razer Blackwidow V3 Pro cơ học đầu tiên và mang tính biểu tượng nhất trên thế giới tạo nên sự phát triển mang tính bước ngoặt. Bước vào một meta không dây mới với Razer BlackWidow V3 Pro — với 3 chế độ kết nối mang lại tính linh hoạt vô song và trải nghiệm chơi game thỏa mãn bao gồm các công tắc tốt nhất trong lớp và các phím có chiều cao đầy đủ. Razer Blackwidow V3 Pro Green Switch - bàn phím bluetooth cơ học được trang bị công nghệ không dây tiên tiến nhất của chúng tôi để chơi game có độ trễ thấp và đầu vào siêu phản hồi — được thực hiện thông qua giao thức dữ liệu được tối ưu hóa, tần số vô tuyến cực nhanh và chuyển đổi tần số liền mạch trong môi trường ồn ào nhất, bão hòa dữ liệu. Razer Blackwidow V3 Pro Green Switch sử dụng Razer HyperSpeed ​​để có hiệu suất không dây hoàn hảo khi chơi game hoặc chuyển sang Bluetooth và kết nối tối đa 3 thiết bị — chuyển đổi liền mạch giữa chúng chỉ với một công tắc. Bao gồm cáp USB-C có thể tháo rời để sạc trong quá trình sử dụng. Lắng nghe và cảm nhận phản hồi hài lòng trong mỗi lần nhấn phím bạn thực hiện, một trong những chiếc bàn phím cơ giá rẻ với thiết kế nhạy bén, xúc giác cung cấp các điểm khởi động và đặt lại được tối ưu hóa để có độ chính xác và hiệu suất tốt hơn khi chơi game. Bàn phím Razer Blackwidow V3 Pro được thiết kế hoàn toàn rõ ràng của nó cung cấp ánh sáng RGB sáng hơn để hiển thị sự rực rỡ thực sự của những gì Razer Chroma ™ RGB có thể làm — từ các tùy chỉnh ánh sáng sâu đến đắm chìm hơn khi nó phản ứng động với hơn 150 trò chơi tích hợp. Cá nhân hóa bàn phím chơi game RGB này với hơn 16,8 triệu màu và bộ hiệu ứng để bạn lựa chọn. Tận hưởng cảm giác đắm chìm hơn với các hiệu ứng ánh sáng động xảy ra khi bạn chơi game trên hơn 150 tựa game được tích hợp Chroma như Fortnite, Apex Legends, Warframe,... Bàn phím Razer Blackwidow V3 Pro sử dụng quy trình đúc Doubleshot để đảm bảo không bao giờ bị mòn, các keycaps trên bàn phím cơ chơi game không dây này cũng có thành cực dày làm cho chúng cực kỳ cứng chắc để chịu được việc sử dụng nhiều lần.', 'razer_blackwidow_v3_green_switch.jpg', 20, 500000, 4 ),
	   ( N'Tai nghe không dây SteelSeries Arctis Pro Wireless', N'Tai nghe SteelSeries Arctis Pro Wireless với các trình điều khiển loa kết hợp công nghệ mà hãng sản xuất tạo ra được sản phẩm không dây nhưng có độ trễ cực kì ngắn, mang âm thanh lớn tới tai nghe của bạn. Đây là một sự giải pháp dành cho giới game thủ muốn có một chiếc tai nghe không dây chất lượng trong từng trận đấu. Với tính năng âm thanh không dây 2.4G. Tai nghe tạo ra được âm thanh không giới hạn kết hợp với Bluetooth cho các thiết bị di động. Sử dụng cả hai kết nối độc lập tạo nên sự đồng nhất tối đa linh hoạt. Tai nghe gaming giá rẻ Artics Pro kết nối với đầu phát sóng không dây thông qua kết nối SteelSeries 2.4G đã được chứng thực. Điều này khiến cho chiếc tai nghe có âm thanh cực kì tốt và ảnh hưởng cực kì ít khi ở độ xa lên tới 40 feet. Với 2 loại pin đi kèm nhau nghĩa rằng bạn sẽ luôn được sử dụng tai nghe đến “ vô thời hạn “ mà không lo hết pin. Đơn giản chỉ cần sử dụng một chiếc và chiếc kia cắm sạc là bạn sẽ không bao giờ phải đợi hàng giờ để được sử dụng chiếc tai nghe yêu quý này.', 'steelseries_arctis_pro_wireless.png', 0, 100, 4 ),
	   ( N'Màn hình Asus ROG Swift PG65UQ 65" VA 4K 144Hz G-Sync', N'Kích thước 35” độ cong hoàn hảo 1800R, tỷ lệ 21:9 Độ phân giải UWQHD 3440 x 1440, trên tấm nền IPS 10 bit cao cấp cùng công nghệ Quantum dot => mang đến hình ảnh hiển thị trung thực, độ chuẩn xác cao. Công nghệ HDR, công nghệ chấm lượng tử Quantum dot cùng khả năng điều khiển đèn nền LED trên 512-zone khác nhau (nâng cấp so với X27 384-zone) => hình ảnh hiển thị không bị rỗ, mà vô cùng mịn, màu trắng vô cùng trong trẻo, và đen tuyền, đồng thời cho dải màu rộng hơn, sắc nét => độ tương phản cao 2500:1 gấp đôi màn hình thường Khả năng hiển thị 1.07 tỷ màu, chuẩn 90% DCI-P3: dải màu chuẩn trong công nghệ ảnh kỹ thuật số, rộng hơn dải màu sRGB 25%. Đạt chuẩn hiển thị VESA Display 1000, mang đến độ sáng tối đa 1000nits, gấp 4 lần màn hình bình thường Công nghệ HDR mang lại hình ảnh có nét tương phản cao hơn cùng màu sắc sống động hơn. Khả năng Overclock lên đến 200Hz - tần số quét đỉnh cao cùng thời gian phản hồi cực nhanh chỉ 2ms (G-to-G) kết hợp công nghệ G-sync Ultimate cho trải nghiệm game mượt mà, sống động nhất, kể cả game có nội dung HDR. Độ sáng tối ưu 1000nits, giúp nhìn rõ ngay cả những vùng tối nhất trong game. Thiết kế đèn Ambient light hình đôi cánh độc quyền trên Predator X35, tạo ra những dải ánh sáng phong cách đầy màu sắc, mang lại góc máy cực cá tính. Thiết kế chân đế kim loại cool ngầu cứng cáp tựa lớp áo giáp hầm hố, bắt mắt, với khả năng xoay, gập, điểu chỉnh độ cao và xoay linh hoạt. Phần mềm RGB Light Sense kết hợp với Lightstick, tạo ra các dải ánh sáng RGB chuyển động theo nhịp điệu nhạc hoặc âm lượng của game và phim, nhạc. Với 9 kiểu ánh sáng ngầu chất chơi. 2 loa tích hợp 4W cho âm thanh sống động, thiết kế cách điệu có khe phía sau lưng giúp tản nhiệt tốt.', 'asus_rog_swift_pg65uq_65-inch_va_4k_144hz_g-sync.jpg', 2, 170000000, 4 ),
	   ( N'Ghế Noble Chair Hero Series Real Leather - Black', N'Hero series là dòng ghế có kích thước lớn nhất trong các dòng Noblechair , sở hữu tay vịn mới, cơ sở chỗ ngồi rộng hơn và tựa lưng cao hơn, phù hợp hơn với nhiều đối tượng người sử dụng khác nhau. Với tính năng điều chỉnh độ phồng của lưng ghế thông qua núm vặn sẽ giúp người dùng có thể lựa chọn một tư thế ngồi thoải mái nhất cho riêng mình, tính năng này chỉ thường được tìm thấy ở những chiếc ghế trong những mẫu xe hơi cao cấp. Giờ đây Noble Chair Hero series đã có thêm phiên bản sử dụng chất liệu Da thật cao cấp tăng thêm vẻ sang trọng đẳng cấp và độ bền của sản phẩm.', 'noble_chair_hero_series_real_leather_black.jpg', 10, 25000000, 4 ),
	   ( N'Bàn phím Logitech K120', NULL, 'ban_phim_logitech_k120.jpg', 30, 160000, 4 ),
	   ( N'Chuột Logitech K120', N'Chuột Logitech K120 là một trong những dòng chuột gaming giá rẻ được sử dụng phổ thông nhất hiện nay. Vì dễ dàng cầm giữ, mang lại cảm giác thoải mái, cảm giác và cảm ứng và điều quan trọng nhất là mang lại hiệu suất tuyệt vời.', 'chuot_logitech_k120.jpg', 20, 80000, 4 ),
	   ( N'Tai nghe Logitech G431', NULL, 'tai_nghe_logitech_g431.jpg', 10, 1590000, 4 ),
	   ( N'Chuột Logitech G Pro Hero', N'HERO 16K là một trong những dòng chuột logitech đưuọc trang bị cảm biến chơi game chính xác nhất từ trước tới nay của chúng tôi với độ chính xác thế hệ tiếp theo và cấu trúc toàn diện. HERO 16K có khả năng tạo ra 400+ IPS, 100 - 16,000 DPI, và làm mịn, lọc hay tăng tốc trên toàn bộ dải DPI.', 'chuot_logitech_g-pro_hero.png', 0, 890000, 4 )

--TABLE Combo
INSERT INTO Combo ( ComboName, [_Image], Total )
VALUES ( N'Combo bàn phím và chuột Logitech K120', 'combo_ban_phim_va_chuot_logitech_k120.png', 240000 ),
	   ( 'Combo Logitech A', 'combo_logitech_a.png', 2480000 )

--TABLE Combo_Product
SELECT * FROM dbo.Combo
SELECT * FROM dbo.Product
SELECT * FROM ProductType

INSERT INTO dbo.Combo_Product ( ComboId, ProductId, Quantity, UnitPrice, Amount )
VALUES (1, 16, 1, 160000, 160000 ),
	   (1, 17, 1, 80000, 80000 ),
	   (2, 18, 1, 1590000, 1590000 ),
	   (2, 19, 1, 890000, 890000 )

--TABLE StockLog
DROP FUNCTION FN_GenerateRandomDate

CREATE PROC PROC_InitStockLog
AS
BEGIN
	DECLARE @RandomDate DATE, @FromDate DATE, @ToDate DATE
	DECLARE @Iteration INT
	DECLARE @ProductRows INT
	
	SET @FromDate = '2020-11-30'
	SET @ToDate = '2020-12-13'
	SET @ProductRows = ( SELECT COUNT(*) FROM dbo.Product )
	SET @Iteration = 1

	WHILE @Iteration <= @ProductRows
	BEGIN
		SET @RandomDate = DATEADD(DAY, RAND(CHECKSUM(NEWID())) * (1 + DATEDIFF(DAY, @FromDate, @ToDate)), @FromDate)

		INSERT INTO dbo.StockLog( ProductId, Stock, [_TimeStamp] )
		SELECT p.Id, p.Stock, @RandomDate
		FROM dbo.Product AS p
		WHERE p.Id = @Iteration

		SET @Iteration = @Iteration + 1
	END
END

EXECUTE PROC_InitStockLog

SELECT * FROM dbo.StockLog
ORDER BY [_TimeStamp]

UPDATE dbo.StockLog
SET Stock = 10
WHERE Stock = 0

SELECT * FROM Combo
select * from combo_product
select * from customer
use SaleManagement
select * from product


