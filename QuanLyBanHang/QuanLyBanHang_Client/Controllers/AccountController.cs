﻿using Repository.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace QuanLyBanHang_Client.Controllers
{
    [SessionState(SessionStateBehavior.Default)]
    public class AccountController : Controller
    {
        private CustomerService _service;
        public AccountController()
        {
            _service = new CustomerService();
        }

        // GET: Account
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            Customer customer = _service.Login(username, password);
            if (customer == null)
            {
                string message = "Tên đăng nhập hoặc mật khẩu không hợp lệ";
                return Json(new { message = message });
            }
            else
            {
                Session["name"] = customer.FirstName + " " + customer.LastName;
                Session["username"] = customer.Email;
                Session["customerId"] = Convert.ToString(customer.Id);
                Session["address"] = customer.C_Address;
                string message = "True";
                return Json(new { message = message });
            }
        }

        [HttpPost]
        public ActionResult Register(string firstname, string lastname, DateTime birthdate, string gender, string email, string address, string password)
        {
            Customer customer = new Customer { FirstName = firstname, LastName = lastname, Birthdate = birthdate, Gender = gender, Email = email, C_Address = address, C_Password = password };
            bool sucess = _service.Register(customer);
            if(sucess)
            {
                return Json(new { message = "Đăng kí tài khoản thành công", success = true});
            } else
            {
                return Json(new { message = "Email đã tồn tại", success = false });
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}