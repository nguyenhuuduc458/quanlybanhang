﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repository.ViewModels;

namespace QuanLyBanHang_Client.Controllers
{
    public class OrderController : Controller
    {
        private InvoiceService _service;

        public OrderController()
        {
            _service = new InvoiceService();
        }

        // GET: Order
        public ActionResult Index(int? customerId)
        {
            if(Session["username"] == null || customerId == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TrackingOrderModel trackingOrder = _service.getOrdersOfCustomer(customerId.GetValueOrDefault());
            return View(trackingOrder);
        }


        [HttpPost]
        public ActionResult getInvoiceDetail(int invoiceId)
        {

            if (Session["username"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            InvoiceDetailItemModel model = _service.getInvoiceDetail(invoiceId);
            return PartialView("_InvoiceDetail", model);
        }

    }
}