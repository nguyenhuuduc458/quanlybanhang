﻿using Repository.Models;
using Repository.ViewModels;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
    public class CartController : Controller
    {
        private ProductService _service;
        public CartController()
        {
            _service = new ProductService();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Buy(int id, int quantity, bool isCombo)
        {
            if (Session["cart"] == null)
            {
                List<Item> cart = new List<Item>();
                Item item = null;
                if(!isCombo)
                {
                    //find product in db
                    Product product = _service.getProductById(id);
                    if(product == null)
                    {
                        return Json(new { message = "Không tồn tại sản phẩm", success = false, JsonRequestBehavior.AllowGet });
                    } else
                    {
                        //check stock of product 
                        if (quantity > product.Stock)
                        {
                            return Json(new { message = "Số lượng sản phẩm tồn kho hiện không đủ để thực hiện giao dịch", success = false, JsonRequestBehavior.AllowGet });
                        }

                        // create item before add in list cart
                        item = new Item
                        {
                            Product = product,
                            Quantity = quantity,
                            isCombo = false
                           
                        };
                    }
                } else
                {

                    // find combo in db
                    Combo combo = _service.getComboById(id);

                    if(combo == null)
                    {
                        return Json(new { message = "Không tồn tại sản phẩm trong csdl", success = false, JsonRequestBehavior.AllowGet });
                    } else
                    {
                        // check quantity of combo validate
                        if(!_service.checkQuantityProductOfCombo(combo, quantity))
                        {
                            return Json(new { message = "Số lượng sản phẩm không đủ để đặt combo", success = false, JsonRequestBehavior.AllowGet });
                        }

                        //create new item to add in list cart
                        item = new Item
                        {
                            Combo = combo,
                            Quantity = quantity,
                            isCombo = true
                        };
                    }
                }
                cart.Add(item);
                Session["cart"] = cart;
                return Json(new { message = "Thêm sản phẩm thành công", success = true, JsonRequestBehavior.AllowGet });
            }
            else
            {
                List<Item> cart = (List<Item>)Session["cart"];
                int index = isExist(id, isCombo);
                if (index != -1)
                {
                    cart[index].Quantity = cart[index].Quantity + quantity;
                    int quantityItem = cart[index].Quantity;
                    if(isCombo)
                    {
                        // check quantity of combo validate
                        if (!_service.checkQuantityProductOfCombo(cart[index].Combo, quantityItem))
                        {
                            cart[index].Quantity = cart[index].Quantity - quantity;
                            return Json(new { message = "Số lượng sản phẩm không đủ để đặt combo", success = false, JsonRequestBehavior.AllowGet });
                        }
                        
                    } else
                    {
                        // check quantity of product validate
                        var product = _service.getProductById(id);
                        if(product == null )
                        {
                            return Json(new { message = "Số lượng sản phẩm không đủ để đặt combo", success = false, JsonRequestBehavior.AllowGet });
                        } else
                        {
                            if (product.Stock < quantityItem)
                            {
                                cart[index].Quantity = cart[index].Quantity - quantity;
                                return Json(new { message = "Số lượng sản phẩm tồn kho hiện không đủ để thực hiện giao dịch", success = false, JsonRequestBehavior.AllowGet });
                            }
                        }
                    }
                }
                else
                {
                    if(isCombo)
                    {
                        Combo combo = _service.getComboById(id);
                        if(combo == null)
                        {
                            return Json(new { message = "Combo không tồn tại", success = false, JsonRequestBehavior.AllowGet });
                        } else
                        {
                            if (!_service.checkQuantityProductOfCombo(combo, quantity))
                            {
                                return Json(new { message = "Số lượng sản phẩm không đủ để đặt combo", success = false, JsonRequestBehavior.AllowGet });
                            }
                            else
                            {
                                cart.Add(new Item
                                {
                                    Combo = combo,
                                    Quantity = quantity,
                                    isCombo = true
                                });
                            }
                        }
                       
                    }  else
                    {
                        Product product = _service.getProductById(id);
                        if(product == null)
                        {
                            return Json(new { message = "Không tồn tại sản phẩm", success = false, JsonRequestBehavior.AllowGet });
                        } else
                        {
                            if (product.Stock < quantity)
                            {
                                return Json(new { message = "Số lượng sản phẩm không đủ", success = false, JsonRequestBehavior.AllowGet });
                            }
                            else
                            {
                                cart.Add(new Item
                                {
                                    Product = product,
                                    Quantity = quantity,
                                    isCombo = false
                                });
                            }
                        }
                       
                    }
                   
                }
                Session["cart"] = cart;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Remove(int id, bool isCombo)
        {
            List<Item> cart = (List<Item>)Session["cart"];
            int index = isExist(id, isCombo);
            if(index != -1)
            {
                cart.RemoveAt(index);
                if(cart.Count <= 0)
                {
                    Session["cart"] = null;
                } else
                {
                    Session["cart"] = cart;
                }
                return Json(new { message = "Xóa sản phẩm thành công", success = true });
            }
            return Json(new { message = "Xóa sản phẩm thất bại", success = false });
        }

        [HttpPost]
        public ActionResult UpdateCart(int id, int quantity, bool isCombo)
        {
            // update cart
            if(Session["cart"] == null)
            {
                return Json(new { message = "Vui đăng thêm sản phẩm trước khi thanh toán", sucess = false });
            } else
            {
                List<Item> cart = (List<Item>)Session["cart"];
                int index = isExist(id, isCombo);
                var oldQuantity = cart[index].Quantity;
                var newQuantity = quantity;

                if (index != -1)
                {
                    if (isCombo)
                    {
                        // check quantity of combo validate
                        if (!_service.checkQuantityProductOfCombo(cart[index].Combo, newQuantity))
                        {
                            return Json(new { message = "Số lượng sản phẩm không đủ để đặt combo", success = false, value = oldQuantity, JsonRequestBehavior.AllowGet });
                        }

                    }
                    else
                    {
                        // check quantity of product validate
                        var product = _service.getProductById(id);
                        if(product == null)
                        {
                            return Json(new { message = "Số lượng sản phẩm tồn kho hiện không đủ để thực hiện giao dịch", success = false, value = oldQuantity, JsonRequestBehavior.AllowGet });
                        } else
                        {

                            if (product.Stock < newQuantity)
                            {
                                return Json(new { message = "Số lượng sản phẩm tồn kho hiện không đủ để thực hiện giao dịch", success = false, value = oldQuantity, JsonRequestBehavior.AllowGet });
                            }
                        }
                       
                    }
                }

                //udapte cart item when validate
                cart[index].Quantity = newQuantity;
                Session["cart"] = cart;
                return Json(new { message = "Thêm thành công", success = true, value = newQuantity, JsonRequestBehavior.AllowGet });
            }


        }

        private int isExist(int id, bool isCombo)
        {
            List<Item> carts = (List<Item>)Session["cart"];
            for (int i = 0; i < carts.Count; i++)
            {
                if(isCombo && carts[i].isCombo)
                {
                    if (carts[i].Combo.Id == id)
                    {
                        return i;
                    }
                } else if(!carts[i].isCombo)
                {
                    if (carts[i].Product.Id == id)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        //Cart detail information
        public ActionResult Detail()
        {
            return View();
        }

        public ActionResult CheckOut()
        {
            return View();
        }

        public int ParseSessionUserId()
        {
            return Int32.Parse(Session["customerId"].ToString());
        }


        [HttpPost]
        public ActionResult CheckOut(string address)
        {
            if(!string.IsNullOrEmpty(Session["customerId"].ToString()))
            {
                
                List<Item> list = Session["cart"] as List<Item>;
                //lần 1 kiểm tra số lượng tồn của toàn bộ giỏ hàng, trả về json nếu thiếu, set quantity về tối đa, nếu quantity = 0 thì remove khỏi cart
                if(list == null)
                {
                    return Json(new { message = "Thanh toán thất bại", success = false });
                }
                foreach (Item i in list)
                {
                    //Create Invoice Combo Detail
                    if (i.isCombo)
                    {
                        if (!_service.checkQuantityProductOfCombo(i.Combo, i.Quantity))
                        {
                            //Thông báo ko đủ số lượng và set lại quantity cho item
                            int idProduct = _service.getQuantityProductNotEnough(i.Combo, i.Quantity);
                            i.Quantity = _service.getProductById(idProduct).Stock;
                            Product p = _service.getProductById(idProduct);
                            //xóa khỏi giỏ hàng nếu quantity = 0
                            if (i.Quantity == 0)
                                list.Remove(i);
                            Session["cart"] = list;
                            return Json(new
                            {
                                message = "Tạo đơn hàng không thành công: " + p.ProductName + "không đủ số lượng tồn để tạo đơn hàng." +
                                                        " Bạn sẽ được điều hướng trở lại trang chi tiết đơn hàng(số lượng của sản phẩm sẽ được đặt thành tối đa số lượng hiện tại còn trong kho)" +
                                                         ". Xin lỗi vì sự bất tiện này",
                                success = false,
                                JsonRequestBehavior.AllowGet
                            });
                        }
                    }

                    //Create Invoice Product Detail
                    else
                    {
                        if (!_service.checkQuantityProductById(i.Product.Id, i.Quantity))
                        {
                            //Thông báo ko đủ số lượng và set lại quantity cho item
                            i.Quantity = _service.getProductById(i.Product.Id).Stock;
                            Session["cart"] = list;
                            if (i.Quantity == 0)
                                list.Remove(i);
                            return Json(new
                            {
                                message = "Tạo đơn hàng không thành công: " + i.Product.ProductName + "không đủ số lượng tồn để tạo đơn hàng." +
                                                        " Bạn sẽ được điều hướng trở lại trang chi tiết đơn hàng(số lượng của sản phẩm sẽ được đặt thành tối đa số lượng hiện tại còn trong kho)" +
                                                         ". Xin lỗi vì sự bất tiện này",
                                success = false,
                                JsonRequestBehavior.AllowGet
                            });
                        }
                    }
                }
                //lần 2 tạo hóa đơn và chi tiết hóa đơn, viết log, trừ số lượng tồn và trả json
                //Create Invoice
                Invoice invoice = new Invoice();
                invoice.C_Date = DateTime.Now;
                invoice.C_Address = address;
                invoice.CustomerId = ParseSessionUserId();
                invoice.Total = 0;
                invoice.C_Status = "Đang xử lý";
                _service.AddInvoice(invoice);

                foreach (Item i in list)
                {
                    //Create Invoice Combo Detail
                    if (i.isCombo)
                    {
                        InvoiceDetail_Combo detail = new InvoiceDetail_Combo();
                        detail.ComboId = i.Combo.Id;
                        detail.InvoiceId = invoice.Id;
                        detail.Quantity = i.Quantity;
                        detail.UnitPrice = i.Combo.Total;
                        detail.Amount = i.Quantity * i.Combo.Total;

                        _service.AddInvoiceDetailCombo(detail);

                        invoice.Total += detail.Amount;
                    }

                    //Create Invoice Product Detail
                    else
                    {
                        InvoiceDetail_Product detail = new InvoiceDetail_Product();
                        detail.InvoiceId = invoice.Id;
                        detail.ProductId = i.Product.Id;
                        detail.Quantity = i.Quantity;
                        detail.UnitPrice = i.Product.Price;
                        detail.Amount = i.Product.Price * i.Quantity;

                        _service.AddInvoiceDetailProduct(detail);

                        invoice.Total += detail.Amount;
                        
                    }   
                }
                _service.UpdateInvoice(invoice);
                Session.Remove("cart");

                return Json(new { message = "Tạo đơn hàng thành công", success = true, JsonRequestBehavior.AllowGet });
            }

            //Chưa đăng nhập
            else
            {
                return RedirectToAction("Detail","Cart");
            }
            
        }
    }
}