﻿using Repository.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang_Client.Controllers
{
    public class ProductController : Controller
    {
        private ProductService _service;
        public ProductController()
        {
            _service = new ProductService();
        }
      
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int? id)
        {
            if (id == null) return null;
            Product product = _service.getProductById(id);
            return View(product);
        }

        public ActionResult ComboDetail(int? id)
        {
            if (id == null) return null;
            Combo combo = _service.getComboById(id);
            return View(combo);
        }
    }
}