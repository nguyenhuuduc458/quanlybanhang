﻿using Repository.Models;
using Repository.ViewModels;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang_Client.Controllers
{
    public class HomeController : Controller
    {
        private ProductService _service;
        public HomeController()
        {
            _service = new ProductService();
        }

        // GET: Home
        public ActionResult Index()
        {
            IEnumerable<Product> products = _service.getAllProduct(null);
            return View(products);
        }

        [HttpPost] 
        public ActionResult FilterItem(string productName)
        {
            SearchModel searchModel = _service.filterItem(productName);
            if(searchModel.Combos == null && searchModel.Products == null)
            {
                return null;
            }
            
            //
            if(searchModel.isCombo)
            {
                //return combo partial view when find product
                IEnumerable<Combo> combos = searchModel.Combos;
                ViewBag.itemFound = combos != null ? combos.Count() : 0;
                return PartialView("_Combo", combos);
            }  else
            {
                IEnumerable<Product> products = searchModel.Products;
                ViewBag.itemFound = products != null ? products.Count() : 0;
                return PartialView("_Product", products);
            }

        }

        [HttpPost]
        public ActionResult LoadCategoryById(int categoryId)
        {
            if(categoryId != 5)
            {
                IEnumerable<Product> products = _service.getProductByCategoryId(categoryId);
                return PartialView("_Product", products);
            } else
            {
                IEnumerable<Combo> combos = _service.getAllCombo();
                return PartialView("_Combo", combos);
            }
           
        }
    }
}