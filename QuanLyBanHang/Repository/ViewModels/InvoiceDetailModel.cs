﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.ViewModels
{
    public class InvoiceDetailItemModel
    {
        public List<InvoiceDetail_Combo> invoiceDetailCombo { get; set; }
        public List<InvoiceDetail_Product> invoiceDetailProduct { get; set; }
    }
}
