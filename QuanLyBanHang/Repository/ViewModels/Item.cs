﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Repository.ViewModels
{
    public class Item
    {
        public Product Product { get; set; }
        public Combo Combo { get; set; }
        public int Quantity { get; set; }
        public bool isCombo { get; set; }

    }
}