﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.ViewModels
{
    public class SearchModel
    {
        public List<Product> Products { get; set; }
        public List<Combo> Combos { get; set; }
        public bool isCombo { get; set; }
    } 
}
