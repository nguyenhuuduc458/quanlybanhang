﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.ViewModels
{
    public class ComparativeModel
    {
        public int Id { get; set; } // id of product
        public string ProductName { get; set; }
        public int Stock { get; set; } // stock of product
        public int Quantity { get; set; } // quantity of product with combo needed
    }
}
