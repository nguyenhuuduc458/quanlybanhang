﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.ViewModels
{
    public class InvoiceProductModel : InvoiceDetail_Product
    {
        public string ProductName { get; set; }
    }
}
