﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.ViewModels
{
    public class InvoiceComboModel : InvoiceDetail_Combo
    { 
        public string ComboName { get; set; }
    }
}
