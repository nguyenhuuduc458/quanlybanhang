﻿using Repository.Models;

namespace Repository.ViewModels
{
    public class InvoiceModel : Invoice
    {
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        public string Address { get; set; }
        public string FullName { get; internal set; }
    }
}