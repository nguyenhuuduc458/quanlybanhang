﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.ViewModels
{
    public class TrackingOrderModel
    {
        public List<Invoice> orderModel { get; set; }
        public bool isHavingOrder { get; set; }

    }
}
