﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class ProductModel : Product
    {
        public string ProductTypeName { get; set; }
    }
}
