﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class ComboProductRepository : Repository<Combo_Product>
    {
        public ComboProductRepository() : base() {  }
        
        public SaleManagementEntities context
        {
            get { return Context as SaleManagementEntities; }
        }
        public IEnumerable<ComboProductModel> getAllModelByComboId(int comboId)
        {
            var result = from c in context.Combo_Product
                         join p in context.Products
                         on c.ProductId equals p.Id
                         where c.ComboId == comboId
                         select new ComboProductModel
                         {
                             ComboId = c.ComboId,
                             ProductName = p.ProductName,
                             ProductId = c.ProductId,
                             Quantity = c.Quantity,
                             UnitPrice = c.UnitPrice,
                             Amount = c.Amount,
                             Product = c.Product,
                             Combo = c.Combo
                         };
            return result.ToList();
        }
        public bool isExists(int productId, int comboId)
        {
            var result = from c in context.Combo_Product
                         where c.ProductId == productId
                         && c.ComboId == comboId
                         select c;
            return (result.FirstOrDefault() == null) ? false : true;
        }
        public double getTotalAmountByComboId(int comboId)
        {
            var result = from c in context.Combo_Product
                         where c.ComboId == comboId
                         select c;
            return (double)result.AsEnumerable().Sum(c => c.Amount);
        }
    }
}
