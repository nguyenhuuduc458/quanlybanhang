﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class InvoiceDetailComboRepository : Repository<InvoiceDetail_Combo> 
    {
        public InvoiceDetailComboRepository() :base() { }

        public SaleManagementEntities context
        {
            get { return Context as SaleManagementEntities; }
        }
    }
}
