﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class Repository<T> where T : class
    {
        protected readonly DbContext Context;

        public Repository()
        {
            Context = new SaleManagementEntities();
        }
        public void Add(T Entity)
        {
            Context.Set<T>().Add(Entity);
            complete();
        }

        public void AddRange(IEnumerable<T> Entities)
        {
            Context.Set<T>().AddRange(Entities);
            complete();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate);
        }

        public IEnumerable<T> GetAll()
        {
            return Context.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public void Remove(T Entity)
        {
            Context.Set<T>().Remove(Entity);
            complete();
        }

        public void RemoveRange(IEnumerable<T> Entities)
        {
            Context.Set<T>().RemoveRange(Entities);
            complete();
        }

        public void Update(T entity)
        {
            Context.Set<T>().AddOrUpdate(entity);
            complete();
        }

        public void complete()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
    }
}
