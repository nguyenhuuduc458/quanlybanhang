﻿using Repository.Models;
using Repository.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class InvoiceRepository : Repository<Invoice>
    {
        public InvoiceRepository() : base() { }

        public SaleManagementEntities context
        {
            get { return Context as SaleManagementEntities; }
        }

        public List<InvoiceModel> getInvoiceModels()
        {
            var result = from i in context.Invoices
                         join c in context.Customers
                         on i.CustomerId equals c.Id
                         select new InvoiceModel
                         {
                             Id = i.Id,
                             CustomerId = c.Id,
                             C_Address = c.C_Address,
                             C_Date = i.C_Date,
                             Total = i.Total,
                             FullName = c.FirstName + c.LastName,
                             Address = c.C_Address,
                             C_Status = i.C_Status
                         };

            return result.OrderByDescending(x => x.C_Date).ToList();
        }

        public List<InvoiceDetailModel> getInvoiceDetailProductByInvoiceId(int invoiceId)
        {
            List<InvoiceDetailModel> list = new List<InvoiceDetailModel>();
            var product = from idp in context.InvoiceDetail_Product
                         join p in context.Products
                         on idp.ProductId equals p.Id
                         where idp.InvoiceId == invoiceId
                         select new InvoiceDetailModel
                         {
                             InvoiceId = idp.InvoiceId,
                             ItemId = idp.ProductId,
                             Quantity = idp.Quantity,
                             UnitPrice = idp.UnitPrice,
                             Amount = idp.Amount,
                             Name = p.ProductName,
                         };

            var combo = from idp in context.InvoiceDetail_Combo
                        join p in context.Comboes
                        on idp.ComboId equals p.Id
                        where idp.InvoiceId == invoiceId
                        select new InvoiceDetailModel
                        {
                            InvoiceId = idp.InvoiceId,
                            ItemId = idp.ComboId,
                            Quantity = idp.Quantity,
                            UnitPrice = idp.UnitPrice,
                            Amount = idp.Amount,
                            Name = p.ComboName,
                        };
            list.AddRange(product.ToList());
            list.AddRange(combo.ToList());

            return list;
        }
        public TrackingOrderModel getMyOrder(int customerId)
        {
            //var orders = context.Invoices
            //            .Where(t => t.CustomerId == customerId)
            //            .Select(t => new MyOrderModel
            //            {
            //                invoices = t,
            //                invoiceCombo = t.InvoiceDetail_Combo
            //                                .Select(m => new InvoiceComboModel
            //                                {
            //                                    Quantity = m.Quantity,
            //                                    UnitPrice = m.UnitPrice, 
            //                                    Amount = m.Amount,
            //                                    ComboName = m.Combo.ComboName
            //                                }).ToList(),
            //                invoiceProduct = t.InvoiceDetail_Product
            //                                .Select( p => new InvoiceProductModel {
            //                                    ProductName = p.Product.ProductName,
            //                                    Quantity = p.Quantity, 
            //                                    Amount = p.Amount, 
            //                                    UnitPrice = p.UnitPrice
            //                                }).ToList(),
            //            }).ToList();

            ////checking having customer have order 
            //var trackingOrder = new TrackingOrderModel
            //{
            //    orderModel = orders,
            //    isHavingOrder = orders.Count() > 0 ? true : false
            //};

            //return trackingOrder;

            //get specific invoices of customer
            var invoices = context.Invoices.Where(t => t.CustomerId == customerId).OrderByDescending(t => t.C_Date).ToList();

            // checking whether customer have order before;
            var trackingOrder = new TrackingOrderModel
            {
                orderModel = invoices,
                isHavingOrder = invoices.Count() > 0 ? true : false
            };
            return trackingOrder;
        }

        public InvoiceDetailItemModel getInvoiceDetail(int invoiceId)
        {
            var invoiceCombo = context.InvoiceDetail_Combo.Where(t => t.InvoiceId == invoiceId).ToList();
            var invoiceProduct = context.InvoiceDetail_Product.Where(t => t.InvoiceId == invoiceId).ToList();
            return new InvoiceDetailItemModel
            {
                invoiceDetailCombo = invoiceCombo,
                invoiceDetailProduct = invoiceProduct
            };
        }
    }
}
