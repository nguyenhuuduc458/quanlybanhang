﻿using Repository.Models;
using Repository.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class ComboRepository : Repository<Combo>
    {
        public ComboRepository() : base() { }

        public SaleManagementEntities context
        {
            get { return Context as SaleManagementEntities; }
        }

        #region CLIENT
        public IEnumerable<ComparativeModel> getProductOfCombo(int id, int quantity) // return comparative model to compare
        {
            var compares = from c in context.Combo_Product
                           join p in context.Products
                           on c.ProductId equals p.Id
                           where c.ComboId == id
                           select new ComparativeModel
                           {
                               Id = p.Id,
                               ProductName = p.ProductName,
                               Stock = p.Stock,
                               Quantity = quantity * c.Quantity // sớ lượng sản phẩm của combo nhân với số combo 
                           };
            return compares;
        }
        #endregion
    }
}
