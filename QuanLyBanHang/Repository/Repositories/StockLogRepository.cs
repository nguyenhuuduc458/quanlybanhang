﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class StockLogRepository : Repository<StockLog>
    {
        public StockLogRepository() : base () { }

        public SaleManagementEntities context
        {
            get { return Context as SaleManagementEntities; }
        }

        #region Admin
        public List<StockLog> getStockLogByProductIdFromTo(int productId, DateTime fromDate, DateTime toDate)
        {
            var result = from p in context.StockLogs
                         where p.C_TimeStamp >= fromDate && p.C_TimeStamp <= toDate && p.ProductId == productId
                         select p;

            return result.ToList();
        }
        #endregion

        #region Client
        #endregion
        public int getIdByProductIdAndDate(DateTime c_TimeStamp, int productId)
        {
            var result = from l in context.StockLogs
                         where l.ProductId == productId && DbFunctions.TruncateTime(l.C_TimeStamp) == DbFunctions.TruncateTime(c_TimeStamp)
                         select l;
            StockLog log = result.FirstOrDefault();
            if (log == null)
                return 0;
                    return log.Id;

        }
    }
}
