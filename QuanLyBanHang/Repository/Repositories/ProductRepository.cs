﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class ProductRepository : Repository<Product>
    {
        public ProductRepository() : base() { }

        public SaleManagementEntities context
        {
            get { return Context as SaleManagementEntities; }
        }
        public IEnumerable<ProductModel> getAllProductModel(string searchString)
        {
            var result = from p in context.Products
                         join t in context.ProductTypes
                         on p.ProductTypeId equals t.Id
                         where p.ProductName.Contains(searchString)
                         select new ProductModel
                         {
                             Id = p.Id,
                             ProductName = p.ProductName,
                             ProductTypeId = p.ProductTypeId,
                             ProductTypeName = t.ProductTypeName,
                             C_Image = p.C_Image,
                             Stock = p.Stock,
                             Price = p.Price,
                             C_Description = p.C_Description
                         };
            return result.ToList();
        }
        public IEnumerable<ProductModel> getAllProductModel()
        {
            var result = from p in context.Products
                         join t in context.ProductTypes
                         on p.ProductTypeId equals t.Id
                         select new ProductModel
                         {
                             Id = p.Id,
                             ProductName = p.ProductName,
                             ProductTypeId = p.ProductTypeId,
                             ProductTypeName = t.ProductTypeName,
                             C_Image = p.C_Image,
                             Stock = p.Stock,
                             Price = p.Price,
                             C_Description = p.C_Description
                         };
            return result.ToList();
        }

        public bool checkQuantityProductById(int id, int quantity)
        {
            var result = from p in context.Products
                         where p.Id == id && p.Stock >= quantity
                         select p;
            Product product = result.FirstOrDefault();
            if (product == null)
                return false;
            return true;
        }

        #region CLIENT

        #endregion
    }
}
