﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class CustomerRepository : Repository<Customer>
    {
        public CustomerRepository() : base() { }

        public SaleManagementEntities context
        {
            get { return Context as SaleManagementEntities; }
        }
    }
}
