﻿using Repository.Models;
using Repository.Repositories;
using Repository.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ProductService
    {
        private ProductRepository productRepository;
        private ComboRepository comboRepository;
        private InvoiceDetailComboRepository invoiceDetailComboRepository;
        private InvoiceDetailProductRepository invoiceDetailProductRepository;
        private StockLogRepository stockLogRepository;
        private InvoiceRepository invoiceRepository;

        public ProductService()
        {
            productRepository = new ProductRepository();
            comboRepository = new ComboRepository();
            invoiceDetailComboRepository = new InvoiceDetailComboRepository();
            invoiceDetailProductRepository = new InvoiceDetailProductRepository();
            stockLogRepository = new StockLogRepository();
            invoiceRepository = new InvoiceRepository();
        }
        #region CLIENT
        public IEnumerable<Product> getAllProduct(string searchString)
        {
            Expression<Func<Product, bool>> predicate = p => true;
            if(searchString != null && !searchString.Equals(""))
            {
                predicate = p => p.ProductName.ToLower().Contains(searchString.ToLower());
            }
            return productRepository.Find(predicate).ToList();
        }
        public IEnumerable<ProductModel> getAllProductModel(string searchString)
        {
            if (searchString!=null && !searchString.Equals(""))
                return productRepository.getAllProductModel(searchString);
            return productRepository.getAllProductModel();
        }

        public IEnumerable<Product> getProductByCategoryId(int productTypeId)
        {
            if (productTypeId == 0)
            {
                return productRepository.GetAll().ToList();
            }
            else  // get product
            {
                Expression<Func<Product, bool>> predicate = p => p.ProductTypeId == productTypeId;
                return productRepository.Find(predicate).ToList();
            }
        }

        public IEnumerable<Combo> getAllCombo()
        {
            return comboRepository.GetAll().ToList();
        }

        public Product getProductById(int? id)
        {
            if (id == null)
                return null;
            return productRepository.GetById(id.GetValueOrDefault());
        }

        public Combo getComboById(int? id)
        {
            if (id == null)
                return null;
            return comboRepository.GetById(id.GetValueOrDefault());
        }

        
        public SearchModel filterItem(string productName)
        {
            SearchModel searchModel = null;
            if(productName.ToLower().Contains("Combo".ToLower()))
            {
                var combos = getAllCombo().Where(t => t.ComboName.ToLower().Contains(productName.ToLower())).ToList();
                searchModel = new SearchModel
                {
                    Products = null,
                    Combos = combos.Count() > 0 ? combos : null,
                    isCombo = true
                };
                return searchModel;
            } else
            {
                var proudcts = getAllProduct(productName).ToList();
                searchModel = new SearchModel
                {
                    Products = proudcts.Count() > 0 ? proudcts : null,
                    Combos = null,
                    isCombo = false
                };
                return searchModel;
            }
        }

        public bool checkQuantityProductOfCombo(Combo combo, int quantity) // quantity: số lượng combo
        {
            if (combo == null) return false;
            IEnumerable<ComparativeModel> productOfCombo = comboRepository.getProductOfCombo(combo.Id, quantity);
            if (productOfCombo.Count() <= 0)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < productOfCombo.Count(); i++)
                {
                    if (productOfCombo.ElementAt(i).Quantity > productOfCombo.ElementAt(i).Stock)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public int getQuantityProductNotEnough(Combo combo, int quantity) // quantity: số lượng combo
        {
            IEnumerable<ComparativeModel> productOfCombo = comboRepository.getProductOfCombo(combo.Id, quantity);
                for (int i = 0; i < productOfCombo.Count(); i++)
                {
                    if (productOfCombo.ElementAt(i).Quantity > productOfCombo.ElementAt(i).Stock)
                    {
                        return productOfCombo.ElementAt(i).Id;
                    }
                }
            return -1;
        }

        public List<Product> getProductByComboId(int comboId)
        {
            return comboRepository.GetById(comboId).Combo_Product.Select(x => x.Product).ToList();
        }

        public void AddInvoiceDetailCombo(InvoiceDetail_Combo detail)
        {
            invoiceDetailComboRepository.Add(detail);
            //Write stock log
            foreach(Product p in getProductByComboId(detail.ComboId))
            {
                StockLog log = new StockLog();
                log.C_TimeStamp = DateTime.Now;
                log.ProductId = p.Id;
                //is exist product on date
                int idLog = stockLogRepository.getIdByProductIdAndDate(log.C_TimeStamp, log.ProductId);
                if(idLog!= 0)
                {
                    log = stockLogRepository.GetById(idLog);
                    log.Stock = p.Stock - detail.Quantity;
                    stockLogRepository.Update(log);
                }
                else
                {
                    log.Stock = p.Stock - detail.Quantity;
                    stockLogRepository.Add(log);
                }
                p.Stock = log.Stock;
                //Update Product'sStock 
                productRepository.Update(p);
            }
        }

       

        public void AddInvoiceDetailProduct(InvoiceDetail_Product detail)
        {
            //check
            invoiceDetailProductRepository.Add(detail);
            //WriteLog
            StockLog log = new StockLog();
            log.C_TimeStamp = DateTime.Now;
            log.ProductId = detail.ProductId;
            int idLog = stockLogRepository.getIdByProductIdAndDate(log.C_TimeStamp, log.ProductId);
            Product p = productRepository.GetById(detail.ProductId);
            if (idLog != 0)
                {
                    log = stockLogRepository.GetById(idLog);
                    log.Stock = p.Stock - detail.Quantity;
                    stockLogRepository.Update(log);
                }
                else
                {
                    log.Stock = p.Stock - detail.Quantity;
                    stockLogRepository.Add(log);
                }
            p.Stock = log.Stock;
            //Update Product'sStock 
            productRepository.Update(p);
        }
        public void AddInvoice(Invoice i)
        {
            invoiceRepository.Add(i);
        }

        public void UpdateInvoice(Invoice i)
        {
            invoiceRepository.Update(i);
        }
        public bool checkQuantityProductById(int id, int quantity)
        {
            return productRepository.checkQuantityProductById(id, quantity);
        }

        #endregion
        #region ADMIN
        public void Remove(Product product)
        {
            productRepository.Remove(product);
        }

        public void Update(Product product)
        {
            productRepository.Update(product);
        }

        public void Add(Product product)
        {
            productRepository.Add(product);
        }

        #endregion
    }
}
