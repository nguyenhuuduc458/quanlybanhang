﻿using Repository.Models;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class StockLogService
    {
        private StockLogRepository stockLogRepository;
        private ProductRepository productRepository;

        public StockLogService()
        {
            stockLogRepository = new StockLogRepository();
            productRepository = new ProductRepository();
        }

        #region Admin
        public List<Product> GetAllProduct()
        {
            return productRepository.GetAll().ToList();
        }

        public List<StockLog> getStockLogByProductIdFromTo(int productId, DateTime from, DateTime to)
        {
            return stockLogRepository.getStockLogByProductIdFromTo(productId, from, to);
        }

        public int getIdByProductIdAndDate(DateTime date, int id)
        {
            return stockLogRepository.getIdByProductIdAndDate(date, id);
        }

        public void AddStockLog(StockLog s)
        {
            stockLogRepository.Add(s);
        }

        public void UpdateStockLog(StockLog s)
        {
            stockLogRepository.Update(s);
        }
        #endregion

        #region Client

        #endregion


    }
}
