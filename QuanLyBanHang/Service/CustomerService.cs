﻿using Repository;
using Repository.Models;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class CustomerService
    {
        private CustomerRepository customerRepository;
        public CustomerService()
        {
            customerRepository = new CustomerRepository();
        }

        public string GetMd5hash(MD5 md5hash, string input)
        {
            byte[] data = md5hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public bool VerifyMd5Hash(MD5 md5hash, string input, string hash)
        {
            var hashOfInput = GetMd5hash(md5hash, input);
            if (String.Compare(hash, hashOfInput) == 0)
            {
                return true;
            }
            return false;
        }

        public Customer Login(string username, string password)
        {
            MD5 md5 = MD5.Create();
            var hashPassword = GetMd5hash(md5, password);

            Expression<Func<Customer, bool>> predicate = m => m.Email.Equals(username)
                    && m.C_Password.ToLower().Equals(hashPassword.ToLower());
            Customer customer = customerRepository.Find(predicate).FirstOrDefault();

            return customer == null ? null : customer;
        }

        public bool Register(Customer customer)
        {
            if (customer == null) return false;
            if(isExistEmail(customer.Email))
            {
                return false;
            }
            //hash password before save info 
            MD5 md5 = MD5.Create();
            var hashPassword = GetMd5hash(md5, customer.C_Password);
            customer.C_Password = hashPassword;
            customerRepository.complete();

            customerRepository.Add(customer);
            return true;
        }

        public bool isExistEmail(string username)
        {
            var customers = customerRepository.GetAll();
            foreach(var item in customers)
            {
                if(item.Email.Equals(username))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
