﻿using Repository.Models;
using Repository.Repositories;
using Repository.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class InvoiceService
    {
        private InvoiceRepository invoiceRepository;
        private InvoiceDetailComboRepository invoiceDetailComboRepository;
        private InvoiceDetailProductRepository invoiceDetailProductRepository;
        
        public InvoiceService()
        {
            invoiceRepository = new InvoiceRepository();
            invoiceDetailComboRepository = new InvoiceDetailComboRepository();
            invoiceDetailProductRepository = new InvoiceDetailProductRepository();
        }

        #region CLIENT
        public TrackingOrderModel getOrdersOfCustomer  (int customerId)
        {
            return invoiceRepository.getMyOrder(customerId);
        }

        public InvoiceDetailItemModel getInvoiceDetail(int invoiceId)
        {
            return invoiceRepository.getInvoiceDetail(invoiceId);
        }
        #endregion

        #region ADMIN
        public List<InvoiceModel> getInvoiceModels()
        {
            return invoiceRepository.getInvoiceModels();
        }

        public List<InvoiceDetailModel> getInvoiceDetailProductModelsByInvoiceId(int invoiceId)
        {
            return invoiceRepository.getInvoiceDetailProductByInvoiceId(invoiceId);
        }

        public Invoice GetInvoiceById(int id)
        {
            return invoiceRepository.GetById(id);
        }

        public void InvoiceUpdate(Invoice invoice)
        {
            invoiceRepository.Update(invoice);
        }
        #endregion

    }
}
