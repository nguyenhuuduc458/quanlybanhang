﻿using Repository.Models;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ProductTypeService
    {
        private ProductTypeRepository productTypeRepository;
        public ProductTypeService()
        {
            productTypeRepository = new ProductTypeRepository();
        }
        #region CLIENT
        public IEnumerable<ProductType> getAllProductType(string searchString)
        {
            Expression<Func<ProductType, bool>> predicate = p => true;
            if(searchString != null && !searchString.Equals(""))
            {
                predicate = p => p.ProductTypeName.ToLower().Contains(searchString.ToLower());
            }
            return productTypeRepository.Find(predicate).ToList();
        }

        #endregion
        #region ADMIN
        public void Remove(ProductType productType)
        {
            productTypeRepository.Remove(productType);
        }

        public void Update(ProductType productType)
        {
            productTypeRepository.Update(productType);
        }

        public void Add(ProductType productType)
        {
            productTypeRepository.Add(productType);
        }
        #endregion
    }
}
