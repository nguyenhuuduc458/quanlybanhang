﻿using Repository.Models;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ComboService
    {
        private ComboRepository comboRepository;
        private ComboProductRepository comboProductRepository;
        private ProductRepository productRepository; 

        public ComboService()
        {
            comboProductRepository = new ComboProductRepository();
            comboRepository = new ComboRepository();
            productRepository = new ProductRepository();
        }
        #region CLIENT

        #endregion

        #region ADMIN
        public IEnumerable<Product> GetAllProduct()
        {
            return productRepository.GetAll();
        }
        public void Add(Combo combo)
        {
            comboRepository.Add(combo);
        }

        public void Update(Combo combo)
        {
            comboRepository.Update(combo);
        }

        public void AddRangeComboProduct(List<Combo_Product> list)
        {
            comboProductRepository.AddRange(list);
        }

        public void UpdateComboProduct(Combo_Product comboProduct)
        {
            comboProductRepository.Update(comboProduct);
        }

        public void RemoveComboProduct(Combo_Product comboProduct)
        {
            comboProductRepository.Remove(comboProduct);
        }

        public List<Combo_Product> getComboProductByComboId(int comboId)
        {
            Expression<Func<Combo_Product, bool>> predicate = p => p.ComboId == comboId;
            return comboProductRepository.Find(predicate).ToList();
        }
        public IEnumerable<Combo> getAllCombo(string searchString)
        {
            Expression<Func<Combo, bool>> predicate = c => true;
            if (searchString != null && !searchString.Equals(""))
            {
                predicate = c => c.ComboName.ToLower().Contains(searchString.ToLower());
            }
            return comboRepository.Find(predicate).ToList();
        }
        #endregion




    }
}
