﻿using Repository.Models;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ComboProductService
    {
        private ComboProductRepository comboProductRepository;

        public ComboProductService()
        {
            comboProductRepository = new ComboProductRepository();
        }
        #region CLIENT

        #endregion

        #region ADMIN
        public void Add(Combo_Product comboProduct)
        {
            comboProductRepository.Add(comboProduct);
        }

        public void Update(Combo_Product comboProduct)
        {
            comboProductRepository.Update(comboProduct);
        }

        public void AddRangeComboProduct(List<Combo_Product> list)
        {
            comboProductRepository.AddRange(list);
        }
        public void Remove(Combo_Product comboProduct)
        {
            comboProductRepository.Remove(comboProduct);
        }
        //public void UpdateComboProduct(Combo_Product comboProduct)
        //{
        //    comboProductRepository.Update(comboProduct);
        //}
        public IEnumerable<ComboProductModel> getAllModelByComboId(int comboId)
        {
            return comboProductRepository.getAllModelByComboId(comboId);
        }
        public bool isExists(int productId, int comboId)
        {
            return comboProductRepository.isExists(productId, comboId);
        }
        public double getTotalAmountByComboId(int comboId)
        {
            return comboProductRepository.getTotalAmountByComboId(comboId);
        }
        #endregion




    }
}
