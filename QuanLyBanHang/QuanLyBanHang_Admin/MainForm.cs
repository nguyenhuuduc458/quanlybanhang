﻿using QuanLyBanHang_Admin.UserControls;
using QuanLyBanHang_Admin.UserControls.ComboUserControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang_Admin
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            btnProduct_Click(sender, e);
        }

        private void btnProduct_Click(object sender, EventArgs e)
        {
            loadUserControl(contentPanel, new ProductUserControl());
        }

        private void btnCombo_Click(object sender, EventArgs e)
        {
            loadUserControl(contentPanel, new ComboUserControl());
        }

        private void btnInvoice_Click(object sender, EventArgs e)
        {
            loadUserControl(contentPanel, new InvoiceUserControl());
        }

        private void btnStatistic_Click(object sender, EventArgs e)
        {
            loadUserControl(contentPanel, new StockStatisticUserControl());
        }

        // load user control into panel
        private void loadUserControl(Panel panel, Control control)
        {
            panel.Controls.Clear();
            panel.Controls.Add(control);
        }
    }
}
