﻿namespace QuanLyBanHang_Admin.UserControls
{
    partial class ProductUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new MetroFramework.Controls.MetroTabControl();
            this.productTab = new MetroFramework.Controls.MetroTabPage();
            this.productList = new System.Windows.Forms.GroupBox();
            this.dtgProductList = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productTypeIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cImageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsProductModel = new System.Windows.Forms.BindingSource(this.components);
            this.txtSearchProduct = new System.Windows.Forms.TextBox();
            this.lblSearchProduct = new System.Windows.Forms.Label();
            this.productInfo = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdateProduct = new System.Windows.Forms.Button();
            this.btnInsertProduct = new System.Windows.Forms.Button();
            this.txtProductStock = new System.Windows.Forms.TextBox();
            this.lblProductStock = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.txtProductPrice = new System.Windows.Forms.TextBox();
            this.lblProductPrice = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtProductImage = new System.Windows.Forms.TextBox();
            this.lblProductImage = new System.Windows.Forms.Label();
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.lblProductDescription = new System.Windows.Forms.Label();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.lblProductName = new System.Windows.Forms.Label();
            this.cbProductType = new System.Windows.Forms.ComboBox();
            this.bdsProductType = new System.Windows.Forms.BindingSource(this.components);
            this.lblProductType = new System.Windows.Forms.Label();
            this.productTypeTab = new MetroFramework.Controls.MetroTabPage();
            this.productTypeList = new System.Windows.Forms.GroupBox();
            this.dtgProductTypeList = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productTypeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSearchProductType = new System.Windows.Forms.TextBox();
            this.lblSearchProductType = new System.Windows.Forms.Label();
            this.productTypeInfo = new System.Windows.Forms.GroupBox();
            this.btnUpdateProductType = new System.Windows.Forms.Button();
            this.btnInsertProductType = new System.Windows.Forms.Button();
            this.txtProductTypeName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProductTypeId = new System.Windows.Forms.TextBox();
            this.lblProductTypeId = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.productTab.SuspendLayout();
            this.productList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductModel)).BeginInit();
            this.productInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductType)).BeginInit();
            this.productTypeTab.SuspendLayout();
            this.productTypeList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductTypeList)).BeginInit();
            this.productTypeInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.productTab);
            this.tabControl.Controls.Add(this.productTypeTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 1;
            this.tabControl.Size = new System.Drawing.Size(862, 682);
            this.tabControl.TabIndex = 0;
            this.tabControl.UseSelectable = true;
            // 
            // productTab
            // 
            this.productTab.Controls.Add(this.productList);
            this.productTab.Controls.Add(this.productInfo);
            this.productTab.HorizontalScrollbarBarColor = true;
            this.productTab.HorizontalScrollbarHighlightOnWheel = false;
            this.productTab.HorizontalScrollbarSize = 8;
            this.productTab.Location = new System.Drawing.Point(4, 38);
            this.productTab.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productTab.Name = "productTab";
            this.productTab.Size = new System.Drawing.Size(854, 640);
            this.productTab.TabIndex = 0;
            this.productTab.Text = "Sản phẩm";
            this.productTab.VerticalScrollbarBarColor = true;
            this.productTab.VerticalScrollbarHighlightOnWheel = false;
            this.productTab.VerticalScrollbarSize = 8;
            // 
            // productList
            // 
            this.productList.BackColor = System.Drawing.Color.Transparent;
            this.productList.Controls.Add(this.dtgProductList);
            this.productList.Controls.Add(this.txtSearchProduct);
            this.productList.Controls.Add(this.lblSearchProduct);
            this.productList.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productList.Location = new System.Drawing.Point(2, 2);
            this.productList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productList.Name = "productList";
            this.productList.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productList.Size = new System.Drawing.Size(852, 286);
            this.productList.TabIndex = 3;
            this.productList.TabStop = false;
            this.productList.Text = "Danh sách sản phẩm";
            // 
            // dtgProductList
            // 
            this.dtgProductList.AllowUserToAddRows = false;
            this.dtgProductList.AllowUserToDeleteRows = false;
            this.dtgProductList.AutoGenerateColumns = false;
            this.dtgProductList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgProductList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgProductList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.productNameDataGridViewTextBoxColumn,
            this.productTypeIdDataGridViewTextBoxColumn,
            this.cImageDataGridViewTextBoxColumn,
            this.stockDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.cDescriptionDataGridViewTextBoxColumn});
            this.dtgProductList.DataSource = this.bdsProductModel;
            this.dtgProductList.Location = new System.Drawing.Point(6, 67);
            this.dtgProductList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtgProductList.Name = "dtgProductList";
            this.dtgProductList.ReadOnly = true;
            this.dtgProductList.RowHeadersVisible = false;
            this.dtgProductList.RowTemplate.Height = 24;
            this.dtgProductList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgProductList.Size = new System.Drawing.Size(839, 215);
            this.dtgProductList.TabIndex = 4;
            this.dtgProductList.CurrentCellChanged += new System.EventHandler(this.dtgProductList_CurrentCellChanged);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.idDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.productNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.productNameDataGridViewTextBoxColumn.HeaderText = "Tên SP";
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.productNameDataGridViewTextBoxColumn.Width = 80;
            // 
            // productTypeIdDataGridViewTextBoxColumn
            // 
            this.productTypeIdDataGridViewTextBoxColumn.DataPropertyName = "ProductTypeId";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.productTypeIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.productTypeIdDataGridViewTextBoxColumn.HeaderText = "ProductTypeId";
            this.productTypeIdDataGridViewTextBoxColumn.Name = "productTypeIdDataGridViewTextBoxColumn";
            this.productTypeIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.productTypeIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // cImageDataGridViewTextBoxColumn
            // 
            this.cImageDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cImageDataGridViewTextBoxColumn.DataPropertyName = "C_Image";
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cImageDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.cImageDataGridViewTextBoxColumn.HeaderText = "File ảnh";
            this.cImageDataGridViewTextBoxColumn.Name = "cImageDataGridViewTextBoxColumn";
            this.cImageDataGridViewTextBoxColumn.ReadOnly = true;
            this.cImageDataGridViewTextBoxColumn.Width = 88;
            // 
            // stockDataGridViewTextBoxColumn
            // 
            this.stockDataGridViewTextBoxColumn.DataPropertyName = "Stock";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.stockDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.stockDataGridViewTextBoxColumn.HeaderText = "SL tồn";
            this.stockDataGridViewTextBoxColumn.Name = "stockDataGridViewTextBoxColumn";
            this.stockDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.priceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.priceDataGridViewTextBoxColumn.HeaderText = "Giá";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            this.priceDataGridViewTextBoxColumn.Width = 57;
            // 
            // cDescriptionDataGridViewTextBoxColumn
            // 
            this.cDescriptionDataGridViewTextBoxColumn.DataPropertyName = "C_Description";
            this.cDescriptionDataGridViewTextBoxColumn.HeaderText = "Mô tả";
            this.cDescriptionDataGridViewTextBoxColumn.Name = "cDescriptionDataGridViewTextBoxColumn";
            this.cDescriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bdsProductModel
            // 
            this.bdsProductModel.DataSource = typeof(Repository.Models.ProductModel);
            // 
            // txtSearchProduct
            // 
            this.txtSearchProduct.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchProduct.Location = new System.Drawing.Point(164, 28);
            this.txtSearchProduct.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtSearchProduct.Name = "txtSearchProduct";
            this.txtSearchProduct.Size = new System.Drawing.Size(218, 27);
            this.txtSearchProduct.TabIndex = 2;
            this.txtSearchProduct.TextChanged += new System.EventHandler(this.txtSearchProduct_TextChanged);
            // 
            // lblSearchProduct
            // 
            this.lblSearchProduct.AutoSize = true;
            this.lblSearchProduct.Location = new System.Drawing.Point(4, 30);
            this.lblSearchProduct.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearchProduct.Name = "lblSearchProduct";
            this.lblSearchProduct.Size = new System.Drawing.Size(150, 20);
            this.lblSearchProduct.TabIndex = 3;
            this.lblSearchProduct.Text = "Tìm kiếm sản phẩm:";
            this.lblSearchProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // productInfo
            // 
            this.productInfo.BackColor = System.Drawing.Color.Transparent;
            this.productInfo.Controls.Add(this.btnClear);
            this.productInfo.Controls.Add(this.btnUpdateProduct);
            this.productInfo.Controls.Add(this.btnInsertProduct);
            this.productInfo.Controls.Add(this.txtProductStock);
            this.productInfo.Controls.Add(this.lblProductStock);
            this.productInfo.Controls.Add(this.lblCurrency);
            this.productInfo.Controls.Add(this.txtProductPrice);
            this.productInfo.Controls.Add(this.lblProductPrice);
            this.productInfo.Controls.Add(this.btnBrowse);
            this.productInfo.Controls.Add(this.txtProductImage);
            this.productInfo.Controls.Add(this.lblProductImage);
            this.productInfo.Controls.Add(this.txtProductDescription);
            this.productInfo.Controls.Add(this.lblProductDescription);
            this.productInfo.Controls.Add(this.txtProductName);
            this.productInfo.Controls.Add(this.lblProductName);
            this.productInfo.Controls.Add(this.cbProductType);
            this.productInfo.Controls.Add(this.lblProductType);
            this.productInfo.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productInfo.Location = new System.Drawing.Point(2, 293);
            this.productInfo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productInfo.Name = "productInfo";
            this.productInfo.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productInfo.Size = new System.Drawing.Size(852, 351);
            this.productInfo.TabIndex = 2;
            this.productInfo.TabStop = false;
            this.productInfo.Text = "Thông tin sản phẩm";
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(500, 313);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(74, 27);
            this.btnClear.TabIndex = 18;
            this.btnClear.Text = "Nhập lại";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnUpdateProduct
            // 
            this.btnUpdateProduct.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateProduct.Location = new System.Drawing.Point(400, 313);
            this.btnUpdateProduct.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUpdateProduct.Name = "btnUpdateProduct";
            this.btnUpdateProduct.Size = new System.Drawing.Size(95, 27);
            this.btnUpdateProduct.TabIndex = 17;
            this.btnUpdateProduct.Text = "Sửa đổi";
            this.btnUpdateProduct.UseVisualStyleBackColor = true;
            this.btnUpdateProduct.Click += new System.EventHandler(this.btnUpdateProduct_Click);
            // 
            // btnInsertProduct
            // 
            this.btnInsertProduct.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertProduct.Location = new System.Drawing.Point(300, 313);
            this.btnInsertProduct.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnInsertProduct.Name = "btnInsertProduct";
            this.btnInsertProduct.Size = new System.Drawing.Size(95, 27);
            this.btnInsertProduct.TabIndex = 16;
            this.btnInsertProduct.Text = "Thêm mới";
            this.btnInsertProduct.UseVisualStyleBackColor = true;
            this.btnInsertProduct.Click += new System.EventHandler(this.btnInsertProduct_Click);
            // 
            // txtProductStock
            // 
            this.txtProductStock.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductStock.Location = new System.Drawing.Point(608, 183);
            this.txtProductStock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductStock.Name = "txtProductStock";
            this.txtProductStock.Size = new System.Drawing.Size(160, 27);
            this.txtProductStock.TabIndex = 14;
            // 
            // lblProductStock
            // 
            this.lblProductStock.AutoSize = true;
            this.lblProductStock.Location = new System.Drawing.Point(502, 185);
            this.lblProductStock.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProductStock.Name = "lblProductStock";
            this.lblProductStock.Size = new System.Drawing.Size(103, 20);
            this.lblProductStock.TabIndex = 15;
            this.lblProductStock.Text = "Số lượng tồn:";
            this.lblProductStock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrency.Location = new System.Drawing.Point(772, 155);
            this.lblCurrency.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(23, 15);
            this.lblCurrency.TabIndex = 13;
            this.lblCurrency.Text = "(đ)";
            // 
            // txtProductPrice
            // 
            this.txtProductPrice.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductPrice.Location = new System.Drawing.Point(608, 150);
            this.txtProductPrice.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductPrice.Name = "txtProductPrice";
            this.txtProductPrice.Size = new System.Drawing.Size(160, 27);
            this.txtProductPrice.TabIndex = 11;
            // 
            // lblProductPrice
            // 
            this.lblProductPrice.AutoSize = true;
            this.lblProductPrice.Location = new System.Drawing.Point(502, 152);
            this.lblProductPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProductPrice.Name = "lblProductPrice";
            this.lblProductPrice.Size = new System.Drawing.Size(108, 20);
            this.lblProductPrice.TabIndex = 12;
            this.lblProductPrice.Text = "Giá sản phẩm:";
            this.lblProductPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(772, 33);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(69, 27);
            this.btnBrowse.TabIndex = 10;
            this.btnBrowse.Text = "Chọn...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtProductImage
            // 
            this.txtProductImage.Enabled = false;
            this.txtProductImage.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductImage.Location = new System.Drawing.Point(608, 34);
            this.txtProductImage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductImage.Multiline = true;
            this.txtProductImage.Name = "txtProductImage";
            this.txtProductImage.Size = new System.Drawing.Size(160, 101);
            this.txtProductImage.TabIndex = 9;
            // 
            // lblProductImage
            // 
            this.lblProductImage.AutoSize = true;
            this.lblProductImage.Location = new System.Drawing.Point(540, 37);
            this.lblProductImage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProductImage.Name = "lblProductImage";
            this.lblProductImage.Size = new System.Drawing.Size(67, 20);
            this.lblProductImage.TabIndex = 8;
            this.lblProductImage.Text = "File ảnh:";
            this.lblProductImage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductDescription.Location = new System.Drawing.Point(118, 128);
            this.txtProductDescription.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductDescription.Multiline = true;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtProductDescription.Size = new System.Drawing.Size(353, 151);
            this.txtProductDescription.TabIndex = 5;
            // 
            // lblProductDescription
            // 
            this.lblProductDescription.AutoSize = true;
            this.lblProductDescription.Location = new System.Drawing.Point(5, 128);
            this.lblProductDescription.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProductDescription.Name = "lblProductDescription";
            this.lblProductDescription.Size = new System.Drawing.Size(54, 20);
            this.lblProductDescription.TabIndex = 6;
            this.lblProductDescription.Text = "Mô tả:";
            this.lblProductDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProductName
            // 
            this.txtProductName.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductName.Location = new System.Drawing.Point(118, 69);
            this.txtProductName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductName.Multiline = true;
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(353, 51);
            this.txtProductName.TabIndex = 3;
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Location = new System.Drawing.Point(4, 69);
            this.lblProductName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(110, 20);
            this.lblProductName.TabIndex = 4;
            this.lblProductName.Text = "Tên sản phẩm:";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbProductType
            // 
            this.cbProductType.DataSource = this.bdsProductType;
            this.cbProductType.DisplayMember = "ProductTypeName";
            this.cbProductType.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProductType.FormattingEnabled = true;
            this.cbProductType.Location = new System.Drawing.Point(118, 35);
            this.cbProductType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbProductType.Name = "cbProductType";
            this.cbProductType.Size = new System.Drawing.Size(182, 27);
            this.cbProductType.TabIndex = 1;
            this.cbProductType.ValueMember = "Id";
            // 
            // bdsProductType
            // 
            this.bdsProductType.DataSource = typeof(Repository.Models.ProductType);
            // 
            // lblProductType
            // 
            this.lblProductType.AutoSize = true;
            this.lblProductType.Location = new System.Drawing.Point(4, 37);
            this.lblProductType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProductType.Name = "lblProductType";
            this.lblProductType.Size = new System.Drawing.Size(114, 20);
            this.lblProductType.TabIndex = 2;
            this.lblProductType.Text = "Loại sản phẩm:";
            this.lblProductType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // productTypeTab
            // 
            this.productTypeTab.Controls.Add(this.productTypeList);
            this.productTypeTab.Controls.Add(this.productTypeInfo);
            this.productTypeTab.HorizontalScrollbarBarColor = true;
            this.productTypeTab.HorizontalScrollbarHighlightOnWheel = false;
            this.productTypeTab.HorizontalScrollbarSize = 8;
            this.productTypeTab.Location = new System.Drawing.Point(4, 38);
            this.productTypeTab.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productTypeTab.Name = "productTypeTab";
            this.productTypeTab.Size = new System.Drawing.Size(854, 640);
            this.productTypeTab.TabIndex = 1;
            this.productTypeTab.Text = "Loại sản phẩm";
            this.productTypeTab.VerticalScrollbarBarColor = true;
            this.productTypeTab.VerticalScrollbarHighlightOnWheel = false;
            this.productTypeTab.VerticalScrollbarSize = 8;
            // 
            // productTypeList
            // 
            this.productTypeList.BackColor = System.Drawing.Color.Transparent;
            this.productTypeList.Controls.Add(this.dtgProductTypeList);
            this.productTypeList.Controls.Add(this.txtSearchProductType);
            this.productTypeList.Controls.Add(this.lblSearchProductType);
            this.productTypeList.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productTypeList.Location = new System.Drawing.Point(318, 2);
            this.productTypeList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productTypeList.Name = "productTypeList";
            this.productTypeList.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productTypeList.Size = new System.Drawing.Size(536, 644);
            this.productTypeList.TabIndex = 5;
            this.productTypeList.TabStop = false;
            this.productTypeList.Text = "Danh sách loại sản phẩm";
            // 
            // dtgProductTypeList
            // 
            this.dtgProductTypeList.AllowUserToAddRows = false;
            this.dtgProductTypeList.AllowUserToDeleteRows = false;
            this.dtgProductTypeList.AutoGenerateColumns = false;
            this.dtgProductTypeList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgProductTypeList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgProductTypeList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgProductTypeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductTypeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.productTypeNameDataGridViewTextBoxColumn});
            this.dtgProductTypeList.DataSource = this.bdsProductType;
            this.dtgProductTypeList.Location = new System.Drawing.Point(4, 58);
            this.dtgProductTypeList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtgProductTypeList.Name = "dtgProductTypeList";
            this.dtgProductTypeList.RowHeadersVisible = false;
            this.dtgProductTypeList.RowTemplate.Height = 24;
            this.dtgProductTypeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgProductTypeList.Size = new System.Drawing.Size(527, 583);
            this.dtgProductTypeList.TabIndex = 4;
            this.dtgProductTypeList.CurrentCellChanged += new System.EventHandler(this.dtgProductTypeList_CurrentCellChanged);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.idDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.idDataGridViewTextBoxColumn1.HeaderText = "Mã loại sản phẩm";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            // 
            // productTypeNameDataGridViewTextBoxColumn
            // 
            this.productTypeNameDataGridViewTextBoxColumn.DataPropertyName = "ProductTypeName";
            this.productTypeNameDataGridViewTextBoxColumn.HeaderText = "Tên loại sản phẩm";
            this.productTypeNameDataGridViewTextBoxColumn.Name = "productTypeNameDataGridViewTextBoxColumn";
            // 
            // txtSearchProductType
            // 
            this.txtSearchProductType.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchProductType.Location = new System.Drawing.Point(188, 28);
            this.txtSearchProductType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtSearchProductType.Name = "txtSearchProductType";
            this.txtSearchProductType.Size = new System.Drawing.Size(218, 27);
            this.txtSearchProductType.TabIndex = 2;
            this.txtSearchProductType.TextChanged += new System.EventHandler(this.txtSearchProductType_TextChanged);
            // 
            // lblSearchProductType
            // 
            this.lblSearchProductType.AutoSize = true;
            this.lblSearchProductType.Location = new System.Drawing.Point(4, 30);
            this.lblSearchProductType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearchProductType.Name = "lblSearchProductType";
            this.lblSearchProductType.Size = new System.Drawing.Size(179, 20);
            this.lblSearchProductType.TabIndex = 3;
            this.lblSearchProductType.Text = "Tìm kiếm loại sản phẩm:";
            this.lblSearchProductType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // productTypeInfo
            // 
            this.productTypeInfo.BackColor = System.Drawing.Color.Transparent;
            this.productTypeInfo.Controls.Add(this.btnUpdateProductType);
            this.productTypeInfo.Controls.Add(this.btnInsertProductType);
            this.productTypeInfo.Controls.Add(this.txtProductTypeName);
            this.productTypeInfo.Controls.Add(this.label7);
            this.productTypeInfo.Controls.Add(this.txtProductTypeId);
            this.productTypeInfo.Controls.Add(this.lblProductTypeId);
            this.productTypeInfo.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productTypeInfo.Location = new System.Drawing.Point(2, 2);
            this.productTypeInfo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productTypeInfo.Name = "productTypeInfo";
            this.productTypeInfo.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.productTypeInfo.Size = new System.Drawing.Size(311, 158);
            this.productTypeInfo.TabIndex = 4;
            this.productTypeInfo.TabStop = false;
            this.productTypeInfo.Text = "Thông tin loại sản phẩm";
            // 
            // btnUpdateProductType
            // 
            this.btnUpdateProductType.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateProductType.Location = new System.Drawing.Point(160, 111);
            this.btnUpdateProductType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUpdateProductType.Name = "btnUpdateProductType";
            this.btnUpdateProductType.Size = new System.Drawing.Size(95, 27);
            this.btnUpdateProductType.TabIndex = 17;
            this.btnUpdateProductType.Text = "Sửa đổi";
            this.btnUpdateProductType.UseVisualStyleBackColor = true;
            this.btnUpdateProductType.Click += new System.EventHandler(this.btnUpdateProductType_Click);
            // 
            // btnInsertProductType
            // 
            this.btnInsertProductType.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertProductType.Location = new System.Drawing.Point(60, 111);
            this.btnInsertProductType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnInsertProductType.Name = "btnInsertProductType";
            this.btnInsertProductType.Size = new System.Drawing.Size(95, 27);
            this.btnInsertProductType.TabIndex = 16;
            this.btnInsertProductType.Text = "Thêm mới";
            this.btnInsertProductType.UseVisualStyleBackColor = true;
            this.btnInsertProductType.Click += new System.EventHandler(this.btnInsertProductType_Click);
            // 
            // txtProductTypeName
            // 
            this.txtProductTypeName.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductTypeName.Location = new System.Drawing.Point(146, 72);
            this.txtProductTypeName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductTypeName.Name = "txtProductTypeName";
            this.txtProductTypeName.Size = new System.Drawing.Size(162, 27);
            this.txtProductTypeName.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 74);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Tên loại sản phẩm:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProductTypeId
            // 
            this.txtProductTypeId.Enabled = false;
            this.txtProductTypeId.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductTypeId.Location = new System.Drawing.Point(146, 34);
            this.txtProductTypeId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductTypeId.Name = "txtProductTypeId";
            this.txtProductTypeId.Size = new System.Drawing.Size(103, 27);
            this.txtProductTypeId.TabIndex = 1;
            // 
            // lblProductTypeId
            // 
            this.lblProductTypeId.AutoSize = true;
            this.lblProductTypeId.Location = new System.Drawing.Point(4, 37);
            this.lblProductTypeId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProductTypeId.Name = "lblProductTypeId";
            this.lblProductTypeId.Size = new System.Drawing.Size(136, 20);
            this.lblProductTypeId.TabIndex = 1;
            this.lblProductTypeId.Text = "Mã loại sản phẩm:";
            this.lblProductTypeId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ProductUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ProductUserControl";
            this.Size = new System.Drawing.Size(862, 682);
            this.Load += new System.EventHandler(this.ProductUserControl_Load);
            this.tabControl.ResumeLayout(false);
            this.productTab.ResumeLayout(false);
            this.productList.ResumeLayout(false);
            this.productList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductModel)).EndInit();
            this.productInfo.ResumeLayout(false);
            this.productInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductType)).EndInit();
            this.productTypeTab.ResumeLayout(false);
            this.productTypeList.ResumeLayout(false);
            this.productTypeList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductTypeList)).EndInit();
            this.productTypeInfo.ResumeLayout(false);
            this.productTypeInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl tabControl;
        private MetroFramework.Controls.MetroTabPage productTab;
        private System.Windows.Forms.GroupBox productInfo;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label lblProductDescription;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.ComboBox cbProductType;
        private System.Windows.Forms.Label lblProductType;
        private MetroFramework.Controls.MetroTabPage productTypeTab;
        private System.Windows.Forms.GroupBox productList;
        private System.Windows.Forms.TextBox txtSearchProduct;
        private System.Windows.Forms.Label lblSearchProduct;
        private System.Windows.Forms.DataGridView dtgProductList;
        private System.Windows.Forms.Label lblProductImage;
        private System.Windows.Forms.TextBox txtProductStock;
        private System.Windows.Forms.Label lblProductStock;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.TextBox txtProductPrice;
        private System.Windows.Forms.Label lblProductPrice;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtProductImage;
        private System.Windows.Forms.Button btnUpdateProduct;
        private System.Windows.Forms.Button btnInsertProduct;
        private System.Windows.Forms.GroupBox productTypeList;
        private System.Windows.Forms.DataGridView dtgProductTypeList;
        private System.Windows.Forms.TextBox txtSearchProductType;
        private System.Windows.Forms.Label lblSearchProductType;
        private System.Windows.Forms.GroupBox productTypeInfo;
        private System.Windows.Forms.Button btnUpdateProductType;
        private System.Windows.Forms.Button btnInsertProductType;
        private System.Windows.Forms.TextBox txtProductTypeName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtProductTypeId;
        private System.Windows.Forms.Label lblProductTypeId;
        private System.Windows.Forms.BindingSource bdsProductType;
        private System.Windows.Forms.BindingSource bdsProductModel;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productTypeIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cImageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productTypeNameDataGridViewTextBoxColumn;
    }
}
