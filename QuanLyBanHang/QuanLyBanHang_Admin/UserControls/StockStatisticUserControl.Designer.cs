﻿namespace QuanLyBanHang_Admin.UserControls
{
    partial class StockStatisticUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.productList = new System.Windows.Forms.GroupBox();
            this.txtSearchProduct = new System.Windows.Forms.TextBox();
            this.lblSearchProduct = new System.Windows.Forms.Label();
            this.dtgProductList = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cImageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productTypeIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsProduct = new System.Windows.Forms.BindingSource(this.components);
            this.statisticList = new System.Windows.Forms.GroupBox();
            this.btnStatistic = new System.Windows.Forms.Button();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.dtgStockLog = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTimeStampDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsStockLog = new System.Windows.Forms.BindingSource(this.components);
            this.btnWriteStockLog = new System.Windows.Forms.Button();
            this.productList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProduct)).BeginInit();
            this.statisticList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgStockLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsStockLog)).BeginInit();
            this.SuspendLayout();
            // 
            // productList
            // 
            this.productList.BackColor = System.Drawing.Color.Transparent;
            this.productList.Controls.Add(this.btnWriteStockLog);
            this.productList.Controls.Add(this.txtSearchProduct);
            this.productList.Controls.Add(this.lblSearchProduct);
            this.productList.Controls.Add(this.dtgProductList);
            this.productList.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productList.Location = new System.Drawing.Point(6, 3);
            this.productList.Name = "productList";
            this.productList.Size = new System.Drawing.Size(1141, 451);
            this.productList.TabIndex = 5;
            this.productList.TabStop = false;
            this.productList.Text = "Danh sách sản phẩm";
            // 
            // txtSearchProduct
            // 
            this.txtSearchProduct.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchProduct.Location = new System.Drawing.Point(210, 29);
            this.txtSearchProduct.Name = "txtSearchProduct";
            this.txtSearchProduct.Size = new System.Drawing.Size(290, 31);
            this.txtSearchProduct.TabIndex = 5;
            this.txtSearchProduct.TextChanged += new System.EventHandler(this.txtSearchProduct_TextChanged);
            // 
            // lblSearchProduct
            // 
            this.lblSearchProduct.AutoSize = true;
            this.lblSearchProduct.Location = new System.Drawing.Point(6, 32);
            this.lblSearchProduct.Name = "lblSearchProduct";
            this.lblSearchProduct.Size = new System.Drawing.Size(188, 25);
            this.lblSearchProduct.TabIndex = 6;
            this.lblSearchProduct.Text = "Tìm kiếm sản phẩm:";
            this.lblSearchProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtgProductList
            // 
            this.dtgProductList.AllowUserToAddRows = false;
            this.dtgProductList.AllowUserToDeleteRows = false;
            this.dtgProductList.AutoGenerateColumns = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgProductList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgProductList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.productNameDataGridViewTextBoxColumn,
            this.cDescriptionDataGridViewTextBoxColumn,
            this.cImageDataGridViewTextBoxColumn,
            this.stockDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.productTypeIdDataGridViewTextBoxColumn});
            this.dtgProductList.DataSource = this.bdsProduct;
            this.dtgProductList.Location = new System.Drawing.Point(11, 71);
            this.dtgProductList.Name = "dtgProductList";
            this.dtgProductList.RowHeadersVisible = false;
            this.dtgProductList.RowTemplate.Height = 24;
            this.dtgProductList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgProductList.Size = new System.Drawing.Size(1124, 374);
            this.dtgProductList.TabIndex = 4;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            this.idDataGridViewTextBoxColumn.Width = 81;
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            this.productNameDataGridViewTextBoxColumn.HeaderText = "Tên sản phẩm";
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.Width = 600;
            // 
            // cDescriptionDataGridViewTextBoxColumn
            // 
            this.cDescriptionDataGridViewTextBoxColumn.DataPropertyName = "C_Description";
            this.cDescriptionDataGridViewTextBoxColumn.HeaderText = "C_Description";
            this.cDescriptionDataGridViewTextBoxColumn.Name = "cDescriptionDataGridViewTextBoxColumn";
            this.cDescriptionDataGridViewTextBoxColumn.Visible = false;
            this.cDescriptionDataGridViewTextBoxColumn.Width = 81;
            // 
            // cImageDataGridViewTextBoxColumn
            // 
            this.cImageDataGridViewTextBoxColumn.DataPropertyName = "C_Image";
            this.cImageDataGridViewTextBoxColumn.HeaderText = "C_Image";
            this.cImageDataGridViewTextBoxColumn.Name = "cImageDataGridViewTextBoxColumn";
            this.cImageDataGridViewTextBoxColumn.Visible = false;
            this.cImageDataGridViewTextBoxColumn.Width = 82;
            // 
            // stockDataGridViewTextBoxColumn
            // 
            this.stockDataGridViewTextBoxColumn.DataPropertyName = "Stock";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.stockDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.stockDataGridViewTextBoxColumn.HeaderText = "SL tồn (hiện tại)";
            this.stockDataGridViewTextBoxColumn.Name = "stockDataGridViewTextBoxColumn";
            this.stockDataGridViewTextBoxColumn.Width = 223;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "Price";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.Visible = false;
            this.priceDataGridViewTextBoxColumn.Width = 82;
            // 
            // productTypeIdDataGridViewTextBoxColumn
            // 
            this.productTypeIdDataGridViewTextBoxColumn.DataPropertyName = "ProductTypeId";
            this.productTypeIdDataGridViewTextBoxColumn.HeaderText = "ProductTypeId";
            this.productTypeIdDataGridViewTextBoxColumn.Name = "productTypeIdDataGridViewTextBoxColumn";
            this.productTypeIdDataGridViewTextBoxColumn.Visible = false;
            this.productTypeIdDataGridViewTextBoxColumn.Width = 81;
            // 
            // bdsProduct
            // 
            this.bdsProduct.DataSource = typeof(Repository.Models.Product);
            // 
            // statisticList
            // 
            this.statisticList.BackColor = System.Drawing.Color.Transparent;
            this.statisticList.Controls.Add(this.btnStatistic);
            this.statisticList.Controls.Add(this.dtpTo);
            this.statisticList.Controls.Add(this.lblTo);
            this.statisticList.Controls.Add(this.dtpFrom);
            this.statisticList.Controls.Add(this.lblFrom);
            this.statisticList.Controls.Add(this.dtgStockLog);
            this.statisticList.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statisticList.Location = new System.Drawing.Point(6, 460);
            this.statisticList.Name = "statisticList";
            this.statisticList.Size = new System.Drawing.Size(1141, 370);
            this.statisticList.TabIndex = 15;
            this.statisticList.TabStop = false;
            this.statisticList.Text = "Bảng thống kê số lượng tồn";
            // 
            // btnStatistic
            // 
            this.btnStatistic.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStatistic.Location = new System.Drawing.Point(515, 28);
            this.btnStatistic.Name = "btnStatistic";
            this.btnStatistic.Size = new System.Drawing.Size(108, 36);
            this.btnStatistic.TabIndex = 19;
            this.btnStatistic.Text = "Xem";
            this.btnStatistic.UseVisualStyleBackColor = true;
            this.btnStatistic.Click += new System.EventHandler(this.btnStatistic_Click);
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd/MM/yyyy";
            this.dtpTo.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(306, 30);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(187, 31);
            this.dtpTo.TabIndex = 18;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(247, 34);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(53, 25);
            this.lblTo.TabIndex = 17;
            this.lblTo.Text = "Đến:";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpFrom.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(54, 30);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(187, 31);
            this.dtpFrom.TabIndex = 16;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(7, 35);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(41, 25);
            this.lblFrom.TabIndex = 15;
            this.lblFrom.Text = "Từ:";
            this.lblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtgStockLog
            // 
            this.dtgStockLog.AllowUserToAddRows = false;
            this.dtgStockLog.AllowUserToDeleteRows = false;
            this.dtgStockLog.AutoGenerateColumns = false;
            this.dtgStockLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgStockLog.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgStockLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgStockLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.productIdDataGridViewTextBoxColumn,
            this.cTimeStampDataGridViewTextBoxColumn,
            this.stockDataGridViewTextBoxColumn1});
            this.dtgStockLog.DataSource = this.bdsStockLog;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgStockLog.DefaultCellStyle = dataGridViewCellStyle10;
            this.dtgStockLog.Location = new System.Drawing.Point(11, 71);
            this.dtgStockLog.Name = "dtgStockLog";
            this.dtgStockLog.RowHeadersVisible = false;
            this.dtgStockLog.RowTemplate.Height = 24;
            this.dtgStockLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgStockLog.Size = new System.Drawing.Size(1124, 285);
            this.dtgStockLog.TabIndex = 4;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // productIdDataGridViewTextBoxColumn
            // 
            this.productIdDataGridViewTextBoxColumn.DataPropertyName = "ProductId";
            this.productIdDataGridViewTextBoxColumn.HeaderText = "ProductId";
            this.productIdDataGridViewTextBoxColumn.Name = "productIdDataGridViewTextBoxColumn";
            this.productIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // cTimeStampDataGridViewTextBoxColumn
            // 
            this.cTimeStampDataGridViewTextBoxColumn.DataPropertyName = "C_TimeStamp";
            dataGridViewCellStyle9.Format = "dd/MM/yyyy";
            dataGridViewCellStyle9.NullValue = null;
            this.cTimeStampDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.cTimeStampDataGridViewTextBoxColumn.HeaderText = "Thời gian thay đổi";
            this.cTimeStampDataGridViewTextBoxColumn.Name = "cTimeStampDataGridViewTextBoxColumn";
            // 
            // stockDataGridViewTextBoxColumn1
            // 
            this.stockDataGridViewTextBoxColumn1.DataPropertyName = "Stock";
            this.stockDataGridViewTextBoxColumn1.HeaderText = "Số lượng tồn";
            this.stockDataGridViewTextBoxColumn1.Name = "stockDataGridViewTextBoxColumn1";
            // 
            // bdsStockLog
            // 
            this.bdsStockLog.DataSource = typeof(Repository.Models.StockLog);
            // 
            // btnWriteStockLog
            // 
            this.btnWriteStockLog.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWriteStockLog.Location = new System.Drawing.Point(958, 21);
            this.btnWriteStockLog.Name = "btnWriteStockLog";
            this.btnWriteStockLog.Size = new System.Drawing.Size(177, 36);
            this.btnWriteStockLog.TabIndex = 20;
            this.btnWriteStockLog.Text = "Cập nhật SL tồn";
            this.btnWriteStockLog.UseVisualStyleBackColor = true;
            this.btnWriteStockLog.Click += new System.EventHandler(this.btnWriteStockLog_Click);
            // 
            // StockStatisticUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statisticList);
            this.Controls.Add(this.productList);
            this.Name = "StockStatisticUserControl";
            this.Size = new System.Drawing.Size(1150, 839);
            this.Load += new System.EventHandler(this.StockStatistic_Load);
            this.productList.ResumeLayout(false);
            this.productList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProduct)).EndInit();
            this.statisticList.ResumeLayout(false);
            this.statisticList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgStockLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsStockLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox productList;
        private System.Windows.Forms.DataGridView dtgProductList;
        private System.Windows.Forms.TextBox txtSearchProduct;
        private System.Windows.Forms.Label lblSearchProduct;
        private System.Windows.Forms.GroupBox statisticList;
        private System.Windows.Forms.DataGridView dtgStockLog;
        private System.Windows.Forms.Button btnStatistic;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.BindingSource bdsProduct;
        private System.Windows.Forms.BindingSource bdsStockLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTimeStampDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cImageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productTypeIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnWriteStockLog;
    }
}
