﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Service;
using Repository.Models;
using System.Drawing.Imaging;
using System.IO;

namespace QuanLyBanHang_Admin.UserControls
{
    public partial class ProductUserControl : UserControl
    {
        #region CUSTOM VARIABLES
        private OpenFileDialog openFileDialog = new OpenFileDialog();
        private ProductService productService = new ProductService();
        private ProductTypeService productTypeService = new ProductTypeService();
        private StockLogService stockLogService = new StockLogService();
        #endregion

        public ProductUserControl()
        {
            InitializeComponent();
        }
        private void loadData()
        {
            bdsProductModel.DataSource = productService.getAllProductModel(null);
            bdsProductType.DataSource = productTypeService.getAllProductType(null);
            cbProductType.DataSource = bdsProductType;
        }

        private void ProductUserControl_Load(object sender, EventArgs e)
        {
            tabControl.SelectedTab = productTab;
            loadData();
        }

        #region PRODUCT

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // to where your opendialog box get starting location
            openFileDialog.InitialDirectory = "C://Desktop";

            // which type image format you want to upload in database
            openFileDialog.Filter = "Image Only(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = Path.GetFullPath(openFileDialog.FileName);
                string fileName = Path.GetFileName(openFileDialog.FileName);
                txtProductImage.Text = fileName;
            }
        }

        private void txtSearchProduct_TextChanged(object sender, EventArgs e)
        {
            bdsProductModel.DataSource = productService.getAllProductModel(txtSearchProduct.Text);
        }

        private void dtgProductList_CurrentCellChanged(object sender, EventArgs e)
        {
            Product product = bdsProductModel.Current as Product;
            if (product != null)
            {
                cbProductType.SelectedValue = product.ProductTypeId;
                txtProductName.Text = product.ProductName;
                txtProductDescription.Text = product.C_Description;
                txtProductImage.Text = product.C_Image;
                txtProductPrice.Text = product.Price.ToString("N0");
                txtProductStock.Text = product.Stock.ToString();
            }
            else
            {
                SetEmpty();
            }
        }
        private void UpdateStockLog(Product product)
        { 
            StockLog s = new StockLog();
            s.C_TimeStamp = DateTime.Now;
            s.ProductId = product.Id;
            s.Stock = product.Stock;
            int idLog = stockLogService.getIdByProductIdAndDate(s.C_TimeStamp, s.ProductId);
            if (idLog == 0)
            {
                stockLogService.AddStockLog(s);
            }
            else
            {
                s.Id = idLog;
                stockLogService.UpdateStockLog(s);
            }
        }

        private void btnInsertProduct_Click(object sender, EventArgs e)
        {
            Product product = new Product();
            if (isValidProduct()) //check input is valid
            {
                //MessageBox.Show(cbProductType.SelectedValue.ToString());
                product.ProductTypeId = Int32.Parse(cbProductType.SelectedValue.ToString());
                product.ProductName = txtProductName.Text;
                product.C_Description = txtProductDescription.Text;
                product.C_Image = txtProductImage.Text;
                product.Price = Double.Parse(txtProductPrice.Text);
                product.Stock = Int32.Parse(txtProductStock.Text);

                // save image to folder
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string saveDirectory = Path.Combine(baseDirectory, "..\\..\\..\\QuanLyBanHang_Client\\Content\\img");
                //string path = Path.GetFullPath(saveDirectory);

                string fileName = Path.GetFileName(openFileDialog.FileName);
                string fileSavePath = Path.Combine(saveDirectory, fileName);
                
                //MessageBox.Show(fileSavePath);

                if (!File.Exists((fileSavePath)))
                    File.Copy(openFileDialog.FileName, Path.GetFullPath(fileSavePath), true);
                productService.Add(product);
                MessageBox.Show("Thêm sản phẩm thành công.");
                loadData();
                UpdateStockLog(product);
            }
        }

        private void btnUpdateProduct_Click(object sender, EventArgs e)
        {
            Product product = bdsProductModel.Current as Product;
            if (isValidProduct())
            {
                product.ProductTypeId = Int32.Parse(cbProductType.SelectedValue.ToString());
                product.ProductName = txtProductName.Text;
                product.C_Description = txtProductDescription.Text;
                product.C_Image = txtProductImage.Text;
                product.Price = Double.Parse(txtProductPrice.Text);
                product.Stock = Int32.Parse(txtProductStock.Text);

                // save image to folder
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string saveDirectory = Path.Combine(baseDirectory, "..\\..\\..\\QuanLyBanHang_Client\\Content\\img");
                //string path = Path.GetFullPath(saveDirectory);

                string fileName = Path.GetFileName(openFileDialog.FileName);
                string fileSavePath = Path.Combine(saveDirectory, fileName);

                //MessageBox.Show(fileSavePath);

                if (!File.Exists((fileSavePath)))
                    File.Copy(openFileDialog.FileName, Path.GetFullPath(fileSavePath), true);

                productService.Update(product);
                MessageBox.Show("Sửa đổi sản phẩm thành công.");
                loadData();
                UpdateStockLog(product);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtProductName.ResetText();
            txtProductDescription.ResetText();
            txtProductImage.ResetText();
            txtProductPrice.ResetText();
            txtProductStock.ResetText();
            txtProductName.Focus();
        }
        #endregion

        #region PRODUCT TYPE
        private void txtSearchProductType_TextChanged(object sender, EventArgs e)
        {
            bdsProductType.DataSource = productTypeService.getAllProductType(txtSearchProductType.Text);
        }

        private void dtgProductTypeList_CurrentCellChanged(object sender, EventArgs e)
        {
            ProductType productType = bdsProductType.Current as ProductType;
            if (productType != null)
            {
                txtProductTypeId.Text = productType.Id.ToString();
                txtProductTypeName.Text = productType.ProductTypeName;
            }
            else
            {
                txtProductTypeId.Text = "";
                txtProductTypeName.Text = "";
            }
        }

        private void btnInsertProductType_Click(object sender, EventArgs e)
        {
            ProductType productType = new ProductType();
            if (isValidProductType())
            {
                productType.ProductTypeName = txtProductTypeName.Text;
                productTypeService.Add(productType);
                MessageBox.Show("Thêm loại sản phẩm thành công.");
                loadData();
            }
        }

        private void btnUpdateProductType_Click(object sender, EventArgs e)
        {
            ProductType productType = bdsProductType.Current as ProductType;
            if (isValidProductType())
            {
                productType.ProductTypeName = txtProductTypeName.Text;
                productTypeService.Update(productType);
                MessageBox.Show("Sửa đổi loại sản phẩm thành công.");
                loadData();
            }
        }
        #endregion

        #region CUSTOM FUNCTIONS

        private void SetEmpty()
        {
            txtProductName.Text = "";
            txtProductDescription.Text = "";
            txtProductImage.Text = "";
            txtProductPrice.Text = "";
            txtProductStock.Text = "";
        }

        private bool isValidProduct()
        {
            string productName = txtProductName.Text;
            string productDescription = txtProductDescription.Text;
            string productImage = txtProductImage.Text;
            string productPrice = txtProductPrice.Text;
            string productStock = txtProductStock.Text;

            if (InputChecker.isEmpty(productName) || InputChecker.isEmpty(productDescription) || InputChecker.isEmpty(productImage)
                || InputChecker.isEmpty(productPrice) || InputChecker.isEmpty(productStock))
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
                return false;
            }
            else if (!InputChecker.isMoney(productPrice))
            {
                MessageBox.Show("Giá sản phẩm không hợp lệ.");
                txtProductPrice.Focus();
                return false;
            }
            else if (!InputChecker.isNumber(productStock) || Int32.Parse(productStock) < 0)
            {
                MessageBox.Show("Số lượng tồn của sản phẩm không hợp lệ.");
                txtProductStock.Focus();
                return false;
            }
            return true;
        }

        private bool isValidProductType()
        {
            string productTypeName = txtProductTypeName.Text;
            
            if (InputChecker.isEmpty(productTypeName))
            {
                MessageBox.Show("Vui lòng nhập tên loại sản phẩm.");
                txtProductTypeName.Focus();
                return false;
            }

            return true;
        }
        #endregion
    }
}
