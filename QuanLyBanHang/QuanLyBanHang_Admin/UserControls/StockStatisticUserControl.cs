﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Service;
using Repository.Models;

namespace QuanLyBanHang_Admin.UserControls
{
    public partial class StockStatisticUserControl : UserControl
    {
        public StockStatisticUserControl()
        {
            InitializeComponent();
        }

        private StockLogService stockLogService = new StockLogService();
        private ProductService productService = new ProductService();
        private List<Product> listProductOrigin = new List<Product>();
        private List<Product> listForSearch = new List<Product>();

        private void StockStatistic_Load(object sender, EventArgs e)
        {
            loadData();
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);   // get the first day of the current month
            var endDate = startDate.AddMonths(1).AddDays(-1);       // get the last day of the current month

            dtpFrom.Value = startDate;
            dtpTo.Value = endDate;
        }

        private void loadData()
        {
            bdsProduct.DataSource = stockLogService.GetAllProduct();
            listProductOrigin = bdsProduct.DataSource as List<Product>;
        }

        #region PRODUCT
        private void txtSearchProduct_TextChanged(object sender, EventArgs e)
        {
            string search = txtSearchProduct.Text;
            if (!String.IsNullOrEmpty(search) )
            {
                listForSearch = listProductOrigin;
                bdsProduct.DataSource = listForSearch.Where(x => x.ProductName.ToUpper().Contains(search.ToUpper()));
            }
            else
            {
                bdsProduct.DataSource = listProductOrigin;
                dtgProductList.Refresh();
            }
        }
        #endregion

        #region STOCK STATISTIC

        private void btnStatistic_Click(object sender, EventArgs e)
        {
            DateTime from = DateTime.Parse(dtpFrom.Value.ToShortDateString());
            DateTime to = DateTime.Parse(dtpTo.Value.ToShortDateString());
            int productId = (bdsProduct.Current as Product).Id;
            int result = DateTime.Compare(from, to);
            if (result <= 0)
            {
                bdsStockLog.DataSource = stockLogService.getStockLogByProductIdFromTo(productId, from, to);
                dtgStockLog.Refresh();
            }
            else
            {
                MessageBox.Show("Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu");
            }
        }


        #endregion

        private void btnWriteStockLog_Click(object sender, EventArgs e)
        {
            foreach(Product p in listProductOrigin)
            {
                StockLog s = new StockLog();
                s.C_TimeStamp = DateTime.Now;
                s.ProductId = p.Id;
                s.Stock = p.Stock;
                int idLog = stockLogService.getIdByProductIdAndDate(s.C_TimeStamp, s.ProductId);
                //chưa có log
                if (idLog == 0)
                {
                    stockLogService.AddStockLog(s);
                }
                else
                {
                    s.Id = idLog;
                    stockLogService.UpdateStockLog(s);
                }
            }
            MessageBox.Show("Cập nhật hàng tồn thành công");

            //
        }
    }
}
