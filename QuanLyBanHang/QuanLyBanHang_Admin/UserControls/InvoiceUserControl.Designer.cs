﻿namespace QuanLyBanHang_Admin.UserControls
{
    partial class InvoiceUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.invoiceList = new System.Windows.Forms.GroupBox();
            this.btnChangeStatus = new System.Windows.Forms.Button();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.dtgInvoiceList = new System.Windows.Forms.DataGridView();
            this.bdsInvoice = new System.Windows.Forms.BindingSource(this.components);
            this.txtSearchInvoice = new System.Windows.Forms.TextBox();
            this.lblSearchInvoice = new System.Windows.Forms.Label();
            this.invoiceDetailInfo = new System.Windows.Forms.GroupBox();
            this.dtgInvoiceDetail = new System.Windows.Forms.DataGridView();
            this.bdsInvoiceDetailModel = new System.Windows.Forms.BindingSource(this.components);
            this.invoiceIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invoiceList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgInvoiceList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsInvoice)).BeginInit();
            this.invoiceDetailInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgInvoiceDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsInvoiceDetailModel)).BeginInit();
            this.SuspendLayout();
            // 
            // invoiceList
            // 
            this.invoiceList.BackColor = System.Drawing.Color.Transparent;
            this.invoiceList.Controls.Add(this.btnChangeStatus);
            this.invoiceList.Controls.Add(this.cbStatus);
            this.invoiceList.Controls.Add(this.lblStatus);
            this.invoiceList.Controls.Add(this.dtgInvoiceList);
            this.invoiceList.Controls.Add(this.txtSearchInvoice);
            this.invoiceList.Controls.Add(this.lblSearchInvoice);
            this.invoiceList.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invoiceList.Location = new System.Drawing.Point(3, 12);
            this.invoiceList.Name = "invoiceList";
            this.invoiceList.Size = new System.Drawing.Size(1144, 481);
            this.invoiceList.TabIndex = 4;
            this.invoiceList.TabStop = false;
            this.invoiceList.Text = "Danh sách hóa đơn";
            // 
            // btnChangeStatus
            // 
            this.btnChangeStatus.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeStatus.Location = new System.Drawing.Point(1000, 24);
            this.btnChangeStatus.Name = "btnChangeStatus";
            this.btnChangeStatus.Size = new System.Drawing.Size(138, 36);
            this.btnChangeStatus.TabIndex = 20;
            this.btnChangeStatus.Text = "Lưu thay đổi";
            this.btnChangeStatus.UseVisualStyleBackColor = true;
            this.btnChangeStatus.Click += new System.EventHandler(this.btnChangeStatus_Click);
            // 
            // cbStatus
            // 
            this.cbStatus.DisplayMember = "ProductTypeName";
            this.cbStatus.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Items.AddRange(new object[] {
            "Đang xử lý",
            "Đã giao"});
            this.cbStatus.Location = new System.Drawing.Point(753, 26);
            this.cbStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(241, 33);
            this.cbStatus.TabIndex = 5;
            this.cbStatus.ValueMember = "Id";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(640, 29);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(107, 25);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "Trạng thái:";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtgInvoiceList
            // 
            this.dtgInvoiceList.AllowUserToAddRows = false;
            this.dtgInvoiceList.AllowUserToDeleteRows = false;
            this.dtgInvoiceList.AutoGenerateColumns = false;
            this.dtgInvoiceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgInvoiceList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgInvoiceList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgInvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgInvoiceList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.customerIdDataGridViewTextBoxColumn,
            this.fullNameDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn,
            this.cDateDataGridViewTextBoxColumn,
            this.cStatusDataGridViewTextBoxColumn,
            this.totalDataGridViewTextBoxColumn});
            this.dtgInvoiceList.DataSource = this.bdsInvoice;
            this.dtgInvoiceList.Location = new System.Drawing.Point(11, 71);
            this.dtgInvoiceList.Name = "dtgInvoiceList";
            this.dtgInvoiceList.RowHeadersVisible = false;
            this.dtgInvoiceList.RowTemplate.Height = 24;
            this.dtgInvoiceList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgInvoiceList.Size = new System.Drawing.Size(1127, 404);
            this.dtgInvoiceList.TabIndex = 4;
            this.dtgInvoiceList.CurrentCellChanged += new System.EventHandler(this.dtgInvoiceList_CurrentCellChanged);
            // 
            // bdsInvoice
            // 
            this.bdsInvoice.DataSource = typeof(Repository.ViewModels.InvoiceModel);
            // 
            // txtSearchInvoice
            // 
            this.txtSearchInvoice.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchInvoice.Location = new System.Drawing.Point(189, 29);
            this.txtSearchInvoice.Name = "txtSearchInvoice";
            this.txtSearchInvoice.Size = new System.Drawing.Size(290, 31);
            this.txtSearchInvoice.TabIndex = 2;
            this.txtSearchInvoice.TextChanged += new System.EventHandler(this.txtSearchInvoice_TextChanged);
            // 
            // lblSearchInvoice
            // 
            this.lblSearchInvoice.AutoSize = true;
            this.lblSearchInvoice.Location = new System.Drawing.Point(6, 32);
            this.lblSearchInvoice.Name = "lblSearchInvoice";
            this.lblSearchInvoice.Size = new System.Drawing.Size(177, 25);
            this.lblSearchInvoice.TabIndex = 3;
            this.lblSearchInvoice.Text = "Tìm kiếm hóa đơn:";
            this.lblSearchInvoice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // invoiceDetailInfo
            // 
            this.invoiceDetailInfo.BackColor = System.Drawing.Color.Transparent;
            this.invoiceDetailInfo.Controls.Add(this.dtgInvoiceDetail);
            this.invoiceDetailInfo.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invoiceDetailInfo.Location = new System.Drawing.Point(6, 499);
            this.invoiceDetailInfo.Name = "invoiceDetailInfo";
            this.invoiceDetailInfo.Size = new System.Drawing.Size(1144, 337);
            this.invoiceDetailInfo.TabIndex = 5;
            this.invoiceDetailInfo.TabStop = false;
            this.invoiceDetailInfo.Text = "Chi tiết hóa đơn";
            // 
            // dtgInvoiceDetail
            // 
            this.dtgInvoiceDetail.AllowUserToAddRows = false;
            this.dtgInvoiceDetail.AllowUserToDeleteRows = false;
            this.dtgInvoiceDetail.AutoGenerateColumns = false;
            this.dtgInvoiceDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgInvoiceDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgInvoiceDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dtgInvoiceDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgInvoiceDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.invoiceIdDataGridViewTextBoxColumn,
            this.ProductName,
            this.quantityDataGridViewTextBoxColumn,
            this.unitPriceDataGridViewTextBoxColumn,
            this.amountDataGridViewTextBoxColumn});
            this.dtgInvoiceDetail.DataSource = this.bdsInvoiceDetailModel;
            this.dtgInvoiceDetail.Location = new System.Drawing.Point(8, 30);
            this.dtgInvoiceDetail.Name = "dtgInvoiceDetail";
            this.dtgInvoiceDetail.RowHeadersVisible = false;
            this.dtgInvoiceDetail.RowTemplate.Height = 24;
            this.dtgInvoiceDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgInvoiceDetail.Size = new System.Drawing.Size(1127, 293);
            this.dtgInvoiceDetail.TabIndex = 5;
            // 
            // bdsInvoiceDetailModel
            // 
            this.bdsInvoiceDetailModel.DataSource = typeof(Repository.ViewModels.InvoiceDetailModel);
            // 
            // invoiceIdDataGridViewTextBoxColumn
            // 
            this.invoiceIdDataGridViewTextBoxColumn.DataPropertyName = "ItemId";
            this.invoiceIdDataGridViewTextBoxColumn.HeaderText = "Mã";
            this.invoiceIdDataGridViewTextBoxColumn.Name = "invoiceIdDataGridViewTextBoxColumn";
            this.invoiceIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // ProductName
            // 
            this.ProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ProductName.DataPropertyName = "Name";
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProductName.DefaultCellStyle = dataGridViewCellStyle6;
            this.ProductName.HeaderText = "Tên";
            this.ProductName.Name = "ProductName";
            this.ProductName.Width = 72;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.quantityDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Số lượng";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // unitPriceDataGridViewTextBoxColumn
            // 
            this.unitPriceDataGridViewTextBoxColumn.DataPropertyName = "UnitPrice";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Format = "N0";
            this.unitPriceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.unitPriceDataGridViewTextBoxColumn.HeaderText = "Đơn giá";
            this.unitPriceDataGridViewTextBoxColumn.Name = "unitPriceDataGridViewTextBoxColumn";
            // 
            // amountDataGridViewTextBoxColumn
            // 
            this.amountDataGridViewTextBoxColumn.DataPropertyName = "Amount";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Format = "N0";
            this.amountDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.amountDataGridViewTextBoxColumn.HeaderText = "Thành tiền";
            this.amountDataGridViewTextBoxColumn.Name = "amountDataGridViewTextBoxColumn";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // customerIdDataGridViewTextBoxColumn
            // 
            this.customerIdDataGridViewTextBoxColumn.DataPropertyName = "CustomerId";
            this.customerIdDataGridViewTextBoxColumn.HeaderText = "CustomerId";
            this.customerIdDataGridViewTextBoxColumn.Name = "customerIdDataGridViewTextBoxColumn";
            this.customerIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // fullNameDataGridViewTextBoxColumn
            // 
            this.fullNameDataGridViewTextBoxColumn.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn.HeaderText = "Tên KH";
            this.fullNameDataGridViewTextBoxColumn.Name = "fullNameDataGridViewTextBoxColumn";
            this.fullNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.addressDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.addressDataGridViewTextBoxColumn.HeaderText = "Địa chỉ";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            // 
            // cDateDataGridViewTextBoxColumn
            // 
            this.cDateDataGridViewTextBoxColumn.DataPropertyName = "C_Date";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "dd/MM/yyyy";
            this.cDateDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.cDateDataGridViewTextBoxColumn.HeaderText = "Ngày lập HĐ";
            this.cDateDataGridViewTextBoxColumn.Name = "cDateDataGridViewTextBoxColumn";
            // 
            // cStatusDataGridViewTextBoxColumn
            // 
            this.cStatusDataGridViewTextBoxColumn.DataPropertyName = "C_Status";
            this.cStatusDataGridViewTextBoxColumn.HeaderText = "Trạng thái";
            this.cStatusDataGridViewTextBoxColumn.Name = "cStatusDataGridViewTextBoxColumn";
            // 
            // totalDataGridViewTextBoxColumn
            // 
            this.totalDataGridViewTextBoxColumn.DataPropertyName = "Total";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.totalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.totalDataGridViewTextBoxColumn.HeaderText = "Tổng tiền";
            this.totalDataGridViewTextBoxColumn.Name = "totalDataGridViewTextBoxColumn";
            // 
            // InvoiceUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.invoiceDetailInfo);
            this.Controls.Add(this.invoiceList);
            this.Name = "InvoiceUserControl";
            this.Size = new System.Drawing.Size(1150, 839);
            this.Load += new System.EventHandler(this.InvoiceUserControl_Load);
            this.invoiceList.ResumeLayout(false);
            this.invoiceList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgInvoiceList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsInvoice)).EndInit();
            this.invoiceDetailInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgInvoiceDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsInvoiceDetailModel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox invoiceList;
        private System.Windows.Forms.DataGridView dtgInvoiceList;
        private System.Windows.Forms.TextBox txtSearchInvoice;
        private System.Windows.Forms.Label lblSearchInvoice;
        private System.Windows.Forms.GroupBox invoiceDetailInfo;
        private System.Windows.Forms.BindingSource bdsInvoiceDetailModel;
        private System.Windows.Forms.DataGridView dtgInvoiceDetail;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.BindingSource bdsInvoice;
        private System.Windows.Forms.Button btnChangeStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoiceIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitPriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn;
    }
}
