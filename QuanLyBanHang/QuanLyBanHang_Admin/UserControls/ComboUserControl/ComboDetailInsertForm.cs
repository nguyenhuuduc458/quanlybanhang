﻿using Repository.Models;
using Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang_Admin.UserControls.ComboUserControl
{
    public partial class ComboDetailInsertForm : Form
    {
        #region CUSTOM VARIABLES
        ProductService productService = new ProductService();
        ComboProductService comboProductService = new ComboProductService();
        ComboService comboService = new ComboService();
        Product currentProduct;
        #endregion

        public ComboDetailInsertForm()
        {
            InitializeComponent();
        }
        public void loadData()
        {
            bdsProduct.DataSource = productService.getAllProduct(null);
        }
        private void ComboDetailInsertForm_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void txtProductSearch_TextChanged(object sender, EventArgs e)
        {
            bdsProduct.DataSource = productService.getAllProduct(txtProductSearch.Text);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (isValid() && ComboUserControl.currentCombo != null && currentProduct != null)
            {
                Combo_Product combo_Product = new Combo_Product();
                combo_Product.ComboId = ComboUserControl.currentCombo.Id;
                combo_Product.ProductId = currentProduct.Id;
                if (comboProductService.isExists(combo_Product.ProductId,combo_Product.ComboId))
                {
                    MessageBox.Show("Sản phẩm này đã có trong combo!");
                    return;
                }
                combo_Product.Quantity = Int32.Parse(txtQuantity.Text);
                if (combo_Product.Quantity>currentProduct.Stock)
                {
                    MessageBox.Show("Số lượng không được lớn hơn số lượng tồn!");
                    return;
                }
                if (combo_Product.Quantity<=0)
                {
                    MessageBox.Show("Số lượng phải lớn hơn 0!");
                    return;
                }
                combo_Product.UnitPrice = currentProduct.Price;
                combo_Product.Amount =  (double)combo_Product.Quantity * combo_Product.UnitPrice;
                comboProductService.Add(combo_Product);
                ComboUserControl.currentCombo.Total += (combo_Product.Amount*90/100);
                comboService.Update(ComboUserControl.currentCombo);
                MessageBox.Show("Thêm sản phẩm vào combo thành công!");
            }
        }

        private void dtgComboList_CurrentCellChanged(object sender, EventArgs e)
        {
            currentProduct = bdsProduct.Current as Product;
        }

        private bool isValid()
        {
            string quantity = txtQuantity.Text;

            if (InputChecker.isEmpty(quantity))
            {
                MessageBox.Show("Vui lòng nhập số lượng cần thêm.");
                txtQuantity.Focus();
                return false;
            }
            else if (!InputChecker.isNumber(quantity) || Int32.Parse(quantity) < 0)
            {
                MessageBox.Show("Số lượng không hợp lệ.");
                txtQuantity.Focus();
                return false;
            }

            return true;
        }
    }
}
