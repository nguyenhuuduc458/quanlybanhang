﻿using Repository.Models;
using Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang_Admin.UserControls.ComboUserControl
{
    public partial class ComboDetailUpdateForm : Form
    {
        ComboProductService comboProductService = new ComboProductService();
        ComboService comboService = new ComboService();
        Product product;
        public ComboDetailUpdateForm()
        {
            InitializeComponent();
        }

        private void ComboDetailUpdateForm_Load(object sender, EventArgs e)
        {
            product = ComboUserControl.currentProduct;
            txtProductName.Text = product.ProductName;
            txtStock.Text = product.Stock.ToString();
            txtQuantity.Text = ComboUserControl.currentComboProduct.Quantity.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            int quantity = Int32.Parse(txtQuantity.Text);
            int stock = Int32.Parse(txtStock.Text);
            if (!InputChecker.isNumber(txtQuantity.Text) && quantity <= 0)
            {
                MessageBox.Show("Số lượng sản phẩm không hợp lệ.");
                txtQuantity.Focus();
                return;
            }
            if (quantity>stock)
            {
                MessageBox.Show("Số lượng sản phẩm phải bé hơn số lượng tồn!");
                txtQuantity.Focus();
                return;
            }
            ComboUserControl.currentCombo.Total -= (ComboUserControl.currentComboProduct.Amount*90/100);
            ComboUserControl.currentComboProduct.Quantity = quantity;
            ComboUserControl.currentComboProduct.Amount = (double)quantity * ComboUserControl.currentComboProduct.UnitPrice;
            ComboUserControl.currentCombo.Total += (ComboUserControl.currentComboProduct.Amount*90/100);
            Combo_Product combo_Product = ComboUserControl.currentComboProduct;
            comboProductService.Update(combo_Product);
            comboService.Update(ComboUserControl.currentCombo);
            MessageBox.Show("Sửa thành công!");
        }
    }
}
