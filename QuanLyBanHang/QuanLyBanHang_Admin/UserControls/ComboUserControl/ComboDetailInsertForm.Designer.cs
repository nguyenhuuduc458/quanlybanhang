﻿namespace QuanLyBanHang_Admin.UserControls.ComboUserControl
{
    partial class ComboDetailInsertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtProductSearch = new System.Windows.Forms.TextBox();
            this.lblSearchProductName = new System.Windows.Forms.Label();
            this.productInfo = new System.Windows.Forms.GroupBox();
            this.dtgComboList = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cImageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productTypeIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsProduct = new System.Windows.Forms.BindingSource(this.components);
            this.lblDefaultQuantity = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.btnInsert = new System.Windows.Forms.Button();
            this.productInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComboList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // txtProductSearch
            // 
            this.txtProductSearch.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductSearch.Location = new System.Drawing.Point(158, 32);
            this.txtProductSearch.Margin = new System.Windows.Forms.Padding(2);
            this.txtProductSearch.Name = "txtProductSearch";
            this.txtProductSearch.Size = new System.Drawing.Size(234, 27);
            this.txtProductSearch.TabIndex = 3;
            this.txtProductSearch.TextChanged += new System.EventHandler(this.txtProductSearch_TextChanged);
            // 
            // lblSearchProductName
            // 
            this.lblSearchProductName.AutoSize = true;
            this.lblSearchProductName.Location = new System.Drawing.Point(16, 34);
            this.lblSearchProductName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearchProductName.Name = "lblSearchProductName";
            this.lblSearchProductName.Size = new System.Drawing.Size(139, 20);
            this.lblSearchProductName.TabIndex = 4;
            this.lblSearchProductName.Text = "Tìm tên sản phẩm:";
            this.lblSearchProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // productInfo
            // 
            this.productInfo.BackColor = System.Drawing.Color.Transparent;
            this.productInfo.Controls.Add(this.dtgComboList);
            this.productInfo.Controls.Add(this.lblDefaultQuantity);
            this.productInfo.Controls.Add(this.txtQuantity);
            this.productInfo.Controls.Add(this.lblQuantity);
            this.productInfo.Controls.Add(this.txtProductSearch);
            this.productInfo.Controls.Add(this.lblSearchProductName);
            this.productInfo.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productInfo.Location = new System.Drawing.Point(10, 8);
            this.productInfo.Margin = new System.Windows.Forms.Padding(2);
            this.productInfo.Name = "productInfo";
            this.productInfo.Padding = new System.Windows.Forms.Padding(2);
            this.productInfo.Size = new System.Drawing.Size(604, 431);
            this.productInfo.TabIndex = 23;
            this.productInfo.TabStop = false;
            this.productInfo.Text = "Danh sách sản phẩm";
            // 
            // dtgComboList
            // 
            this.dtgComboList.AllowUserToAddRows = false;
            this.dtgComboList.AllowUserToDeleteRows = false;
            this.dtgComboList.AutoGenerateColumns = false;
            this.dtgComboList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgComboList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgComboList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgComboList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgComboList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.productNameDataGridViewTextBoxColumn,
            this.cDescriptionDataGridViewTextBoxColumn,
            this.cImageDataGridViewTextBoxColumn,
            this.stockDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.productTypeIdDataGridViewTextBoxColumn});
            this.dtgComboList.DataSource = this.bdsProduct;
            this.dtgComboList.Location = new System.Drawing.Point(8, 103);
            this.dtgComboList.Margin = new System.Windows.Forms.Padding(2);
            this.dtgComboList.Name = "dtgComboList";
            this.dtgComboList.RowHeadersVisible = false;
            this.dtgComboList.RowTemplate.Height = 24;
            this.dtgComboList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgComboList.Size = new System.Drawing.Size(592, 323);
            this.dtgComboList.TabIndex = 8;
            this.dtgComboList.CurrentCellChanged += new System.EventHandler(this.dtgComboList_CurrentCellChanged);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.productNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.productNameDataGridViewTextBoxColumn.HeaderText = "Tên sản phẩm";
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.Width = 131;
            // 
            // cDescriptionDataGridViewTextBoxColumn
            // 
            this.cDescriptionDataGridViewTextBoxColumn.DataPropertyName = "C_Description";
            this.cDescriptionDataGridViewTextBoxColumn.HeaderText = "C_Description";
            this.cDescriptionDataGridViewTextBoxColumn.Name = "cDescriptionDataGridViewTextBoxColumn";
            this.cDescriptionDataGridViewTextBoxColumn.Visible = false;
            // 
            // cImageDataGridViewTextBoxColumn
            // 
            this.cImageDataGridViewTextBoxColumn.DataPropertyName = "C_Image";
            this.cImageDataGridViewTextBoxColumn.HeaderText = "C_Image";
            this.cImageDataGridViewTextBoxColumn.Name = "cImageDataGridViewTextBoxColumn";
            this.cImageDataGridViewTextBoxColumn.Visible = false;
            // 
            // stockDataGridViewTextBoxColumn
            // 
            this.stockDataGridViewTextBoxColumn.DataPropertyName = "Stock";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.stockDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.stockDataGridViewTextBoxColumn.HeaderText = "Số lượng tồn";
            this.stockDataGridViewTextBoxColumn.Name = "stockDataGridViewTextBoxColumn";
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "N0";
            this.priceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.priceDataGridViewTextBoxColumn.HeaderText = "Đơn giá";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            // 
            // productTypeIdDataGridViewTextBoxColumn
            // 
            this.productTypeIdDataGridViewTextBoxColumn.DataPropertyName = "ProductTypeId";
            this.productTypeIdDataGridViewTextBoxColumn.HeaderText = "ProductTypeId";
            this.productTypeIdDataGridViewTextBoxColumn.Name = "productTypeIdDataGridViewTextBoxColumn";
            this.productTypeIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // bdsProduct
            // 
            this.bdsProduct.DataSource = typeof(Repository.Models.Product);
            // 
            // lblDefaultQuantity
            // 
            this.lblDefaultQuantity.AutoSize = true;
            this.lblDefaultQuantity.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefaultQuantity.Location = new System.Drawing.Point(249, 67);
            this.lblDefaultQuantity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDefaultQuantity.Name = "lblDefaultQuantity";
            this.lblDefaultQuantity.Size = new System.Drawing.Size(103, 19);
            this.lblDefaultQuantity.TabIndex = 7;
            this.lblDefaultQuantity.Text = "(mặc định là 1)";
            this.lblDefaultQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtQuantity
            // 
            this.txtQuantity.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantity.Location = new System.Drawing.Point(158, 64);
            this.txtQuantity.Margin = new System.Windows.Forms.Padding(2);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(87, 27);
            this.txtQuantity.TabIndex = 5;
            this.txtQuantity.Text = "1";
            this.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(4, 67);
            this.lblQuantity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(147, 20);
            this.lblQuantity.TabIndex = 6;
            this.lblQuantity.Text = "Số lượng sản phẩm:";
            this.lblQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInsert
            // 
            this.btnInsert.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsert.Location = new System.Drawing.Point(273, 454);
            this.btnInsert.Margin = new System.Windows.Forms.Padding(2);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(92, 27);
            this.btnInsert.TabIndex = 24;
            this.btnInsert.Text = "Thêm";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // ComboDetailInsertForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 488);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.productInfo);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ComboDetailInsertForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm sản phẩm";
            this.Load += new System.EventHandler(this.ComboDetailInsertForm_Load);
            this.productInfo.ResumeLayout(false);
            this.productInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComboList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProduct)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtProductSearch;
        private System.Windows.Forms.Label lblSearchProductName;
        private System.Windows.Forms.GroupBox productInfo;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label lblDefaultQuantity;
        private System.Windows.Forms.DataGridView dtgComboList;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cImageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productTypeIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bdsProduct;
        private System.Windows.Forms.Button btnInsert;
    }
}