﻿namespace QuanLyBanHang_Admin.UserControls.ComboUserControl
{
    partial class ComboUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.comboList = new System.Windows.Forms.GroupBox();
            this.dtgComboList = new System.Windows.Forms.DataGridView();
            this.bdsCombo = new System.Windows.Forms.BindingSource(this.components);
            this.txtSearchCombo = new System.Windows.Forms.TextBox();
            this.lblSearchCombo = new System.Windows.Forms.Label();
            this.comboInfo = new System.Windows.Forms.GroupBox();
            this.btnClearImage = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtComboImage = new System.Windows.Forms.TextBox();
            this.lblComboImage = new System.Windows.Forms.Label();
            this.txtComboName = new System.Windows.Forms.TextBox();
            this.lblComboName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.btnInsertProduct = new System.Windows.Forms.Button();
            this.btnUpdateProductQuantity = new System.Windows.Forms.Button();
            this.dtgComboProduct = new System.Windows.Forms.DataGridView();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsComboProductModel = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cImageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cImageDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComboList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsCombo)).BeginInit();
            this.comboInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComboProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsComboProductModel)).BeginInit();
            this.SuspendLayout();
            // 
            // comboList
            // 
            this.comboList.BackColor = System.Drawing.Color.Transparent;
            this.comboList.Controls.Add(this.dtgComboList);
            this.comboList.Controls.Add(this.txtSearchCombo);
            this.comboList.Controls.Add(this.lblSearchCombo);
            this.comboList.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboList.Location = new System.Drawing.Point(385, 2);
            this.comboList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboList.Name = "comboList";
            this.comboList.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboList.Size = new System.Drawing.Size(763, 396);
            this.comboList.TabIndex = 5;
            this.comboList.TabStop = false;
            this.comboList.Text = "Danh sách combo";
            // 
            // dtgComboList
            // 
            this.dtgComboList.AllowUserToAddRows = false;
            this.dtgComboList.AllowUserToDeleteRows = false;
            this.dtgComboList.AutoGenerateColumns = false;
            this.dtgComboList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgComboList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgComboList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgComboList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgComboList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.comboNameDataGridViewTextBoxColumn1,
            this.cImageDataGridViewTextBoxColumn1,
            this.totalDataGridViewTextBoxColumn1});
            this.dtgComboList.DataSource = this.bdsCombo;
            this.dtgComboList.Location = new System.Drawing.Point(5, 78);
            this.dtgComboList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtgComboList.Name = "dtgComboList";
            this.dtgComboList.RowHeadersVisible = false;
            this.dtgComboList.RowTemplate.Height = 24;
            this.dtgComboList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgComboList.Size = new System.Drawing.Size(745, 313);
            this.dtgComboList.TabIndex = 4;
            this.dtgComboList.CurrentCellChanged += new System.EventHandler(this.dtgComboList_CurrentCellChanged);
            // 
            // bdsCombo
            // 
            this.bdsCombo.DataSource = typeof(Repository.Models.Combo);
            // 
            // txtSearchCombo
            // 
            this.txtSearchCombo.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchCombo.Location = new System.Drawing.Point(187, 39);
            this.txtSearchCombo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSearchCombo.Name = "txtSearchCombo";
            this.txtSearchCombo.Size = new System.Drawing.Size(289, 31);
            this.txtSearchCombo.TabIndex = 2;
            this.txtSearchCombo.TextChanged += new System.EventHandler(this.txtSearchCombo_TextChanged);
            // 
            // lblSearchCombo
            // 
            this.lblSearchCombo.AutoSize = true;
            this.lblSearchCombo.Location = new System.Drawing.Point(4, 42);
            this.lblSearchCombo.Name = "lblSearchCombo";
            this.lblSearchCombo.Size = new System.Drawing.Size(165, 25);
            this.lblSearchCombo.TabIndex = 3;
            this.lblSearchCombo.Text = "Tìm kiếm combo:";
            this.lblSearchCombo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboInfo
            // 
            this.comboInfo.BackColor = System.Drawing.Color.Transparent;
            this.comboInfo.Controls.Add(this.btnClearImage);
            this.comboInfo.Controls.Add(this.btnClear);
            this.comboInfo.Controls.Add(this.btnInsert);
            this.comboInfo.Controls.Add(this.btnUpdate);
            this.comboInfo.Controls.Add(this.btnBrowse);
            this.comboInfo.Controls.Add(this.txtComboImage);
            this.comboInfo.Controls.Add(this.lblComboImage);
            this.comboInfo.Controls.Add(this.txtComboName);
            this.comboInfo.Controls.Add(this.lblComboName);
            this.comboInfo.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboInfo.Location = new System.Drawing.Point(3, 2);
            this.comboInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboInfo.Name = "comboInfo";
            this.comboInfo.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboInfo.Size = new System.Drawing.Size(376, 396);
            this.comboInfo.TabIndex = 24;
            this.comboInfo.TabStop = false;
            this.comboInfo.Text = "Thông tin combo";
            // 
            // btnClearImage
            // 
            this.btnClearImage.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearImage.Location = new System.Drawing.Point(227, 279);
            this.btnClearImage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClearImage.Name = "btnClearImage";
            this.btnClearImage.Size = new System.Drawing.Size(99, 33);
            this.btnClearImage.TabIndex = 17;
            this.btnClearImage.Text = "Bỏ hình";
            this.btnClearImage.UseVisualStyleBackColor = true;
            this.btnClearImage.Click += new System.EventHandler(this.btnClearImage_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(267, 350);
            this.btnClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(99, 33);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "Nhập lại";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsert.Location = new System.Drawing.Point(9, 350);
            this.btnInsert.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(123, 33);
            this.btnInsert.TabIndex = 15;
            this.btnInsert.Text = "Thêm mới";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(139, 350);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(123, 33);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "Sửa đổi";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(129, 279);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(92, 33);
            this.btnBrowse.TabIndex = 10;
            this.btnBrowse.Text = "Chọn...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtComboImage
            // 
            this.txtComboImage.Enabled = false;
            this.txtComboImage.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComboImage.Location = new System.Drawing.Point(129, 146);
            this.txtComboImage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtComboImage.Multiline = true;
            this.txtComboImage.Name = "txtComboImage";
            this.txtComboImage.Size = new System.Drawing.Size(212, 128);
            this.txtComboImage.TabIndex = 9;
            // 
            // lblComboImage
            // 
            this.lblComboImage.AutoSize = true;
            this.lblComboImage.Location = new System.Drawing.Point(37, 146);
            this.lblComboImage.Name = "lblComboImage";
            this.lblComboImage.Size = new System.Drawing.Size(85, 25);
            this.lblComboImage.TabIndex = 8;
            this.lblComboImage.Text = "File ảnh:";
            this.lblComboImage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtComboName
            // 
            this.txtComboName.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComboName.Location = new System.Drawing.Point(129, 39);
            this.txtComboName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtComboName.Multiline = true;
            this.txtComboName.Name = "txtComboName";
            this.txtComboName.Size = new System.Drawing.Size(212, 104);
            this.txtComboName.TabIndex = 3;
            // 
            // lblComboName
            // 
            this.lblComboName.AutoSize = true;
            this.lblComboName.Location = new System.Drawing.Point(8, 39);
            this.lblComboName.Name = "lblComboName";
            this.lblComboName.Size = new System.Drawing.Size(115, 25);
            this.lblComboName.TabIndex = 4;
            this.lblComboName.Text = "Tên combo:";
            this.lblComboName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblCurrency);
            this.groupBox1.Controls.Add(this.lblTotal);
            this.groupBox1.Controls.Add(this.txtTotal);
            this.groupBox1.Controls.Add(this.btnInsertProduct);
            this.groupBox1.Controls.Add(this.btnUpdateProductQuantity);
            this.groupBox1.Controls.Add(this.dtgComboProduct);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 405);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(1144, 420);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách các sản phẩm trong combo";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrency.Location = new System.Drawing.Point(1099, 388);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(33, 23);
            this.lblCurrency.TabIndex = 20;
            this.lblCurrency.Text = "(đ)";
            this.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(771, 386);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(103, 25);
            this.lblTotal.TabIndex = 19;
            this.lblTotal.Text = "Tổng tiền:";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(879, 383);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(215, 31);
            this.txtTotal.TabIndex = 18;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnInsertProduct
            // 
            this.btnInsertProduct.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertProduct.Location = new System.Drawing.Point(5, 41);
            this.btnInsertProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnInsertProduct.Name = "btnInsertProduct";
            this.btnInsertProduct.Size = new System.Drawing.Size(163, 33);
            this.btnInsertProduct.TabIndex = 17;
            this.btnInsertProduct.Text = "Thêm sản phẩm";
            this.btnInsertProduct.UseVisualStyleBackColor = true;
            this.btnInsertProduct.Click += new System.EventHandler(this.btnInsertProduct_Click);
            // 
            // btnUpdateProductQuantity
            // 
            this.btnUpdateProductQuantity.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateProductQuantity.Location = new System.Drawing.Point(175, 41);
            this.btnUpdateProductQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateProductQuantity.Name = "btnUpdateProductQuantity";
            this.btnUpdateProductQuantity.Size = new System.Drawing.Size(307, 33);
            this.btnUpdateProductQuantity.TabIndex = 16;
            this.btnUpdateProductQuantity.Text = "Thay đổi số lượng sản phẩm";
            this.btnUpdateProductQuantity.UseVisualStyleBackColor = true;
            this.btnUpdateProductQuantity.Click += new System.EventHandler(this.btnUpdateProductQuantity_Click);
            // 
            // dtgComboProduct
            // 
            this.dtgComboProduct.AllowUserToAddRows = false;
            this.dtgComboProduct.AllowUserToDeleteRows = false;
            this.dtgComboProduct.AutoGenerateColumns = false;
            this.dtgComboProduct.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgComboProduct.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgComboProduct.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgComboProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgComboProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productNameDataGridViewTextBoxColumn,
            this.comboIdDataGridViewTextBoxColumn,
            this.productIdDataGridViewTextBoxColumn,
            this.unitPriceDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.amountDataGridViewTextBoxColumn});
            this.dtgComboProduct.DataSource = this.bdsComboProductModel;
            this.dtgComboProduct.Location = new System.Drawing.Point(5, 90);
            this.dtgComboProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtgComboProduct.Name = "dtgComboProduct";
            this.dtgComboProduct.ReadOnly = true;
            this.dtgComboProduct.RowHeadersVisible = false;
            this.dtgComboProduct.RowTemplate.Height = 24;
            this.dtgComboProduct.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgComboProduct.Size = new System.Drawing.Size(1127, 287);
            this.dtgComboProduct.TabIndex = 4;
            this.dtgComboProduct.CurrentCellChanged += new System.EventHandler(this.dtgComboProduct_CurrentCellChanged);
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.productNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.productNameDataGridViewTextBoxColumn.HeaderText = "Tên sản phẩm";
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.productNameDataGridViewTextBoxColumn.Width = 162;
            // 
            // comboIdDataGridViewTextBoxColumn
            // 
            this.comboIdDataGridViewTextBoxColumn.DataPropertyName = "ComboId";
            this.comboIdDataGridViewTextBoxColumn.HeaderText = "ComboId";
            this.comboIdDataGridViewTextBoxColumn.Name = "comboIdDataGridViewTextBoxColumn";
            this.comboIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.comboIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // productIdDataGridViewTextBoxColumn
            // 
            this.productIdDataGridViewTextBoxColumn.DataPropertyName = "ProductId";
            this.productIdDataGridViewTextBoxColumn.HeaderText = "ProductId";
            this.productIdDataGridViewTextBoxColumn.Name = "productIdDataGridViewTextBoxColumn";
            this.productIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.productIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // unitPriceDataGridViewTextBoxColumn
            // 
            this.unitPriceDataGridViewTextBoxColumn.DataPropertyName = "UnitPrice";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Format = "N0";
            this.unitPriceDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.unitPriceDataGridViewTextBoxColumn.HeaderText = "Đơn giá";
            this.unitPriceDataGridViewTextBoxColumn.Name = "unitPriceDataGridViewTextBoxColumn";
            this.unitPriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.quantityDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "Số lượng";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            this.quantityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // amountDataGridViewTextBoxColumn
            // 
            this.amountDataGridViewTextBoxColumn.DataPropertyName = "Amount";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Format = "N0";
            this.amountDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.amountDataGridViewTextBoxColumn.HeaderText = "Thành tiền";
            this.amountDataGridViewTextBoxColumn.Name = "amountDataGridViewTextBoxColumn";
            this.amountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bdsComboProductModel
            // 
            this.bdsComboProductModel.DataSource = typeof(Repository.Models.ComboProductModel);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // comboNameDataGridViewTextBoxColumn
            // 
            this.comboNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.comboNameDataGridViewTextBoxColumn.DataPropertyName = "ComboName";
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.comboNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.comboNameDataGridViewTextBoxColumn.HeaderText = "Tên combo";
            this.comboNameDataGridViewTextBoxColumn.Name = "comboNameDataGridViewTextBoxColumn";
            // 
            // cImageDataGridViewTextBoxColumn
            // 
            this.cImageDataGridViewTextBoxColumn.DataPropertyName = "C_Image";
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cImageDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this.cImageDataGridViewTextBoxColumn.HeaderText = "File ảnh";
            this.cImageDataGridViewTextBoxColumn.Name = "cImageDataGridViewTextBoxColumn";
            // 
            // totalDataGridViewTextBoxColumn
            // 
            this.totalDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.totalDataGridViewTextBoxColumn.DataPropertyName = "Total";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.Format = "N0";
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.totalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this.totalDataGridViewTextBoxColumn.HeaderText = "Giá";
            this.totalDataGridViewTextBoxColumn.Name = "totalDataGridViewTextBoxColumn";
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // comboNameDataGridViewTextBoxColumn1
            // 
            this.comboNameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.comboNameDataGridViewTextBoxColumn1.DataPropertyName = "ComboName";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.comboNameDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.comboNameDataGridViewTextBoxColumn1.HeaderText = "Tên combo";
            this.comboNameDataGridViewTextBoxColumn1.Name = "comboNameDataGridViewTextBoxColumn1";
            this.comboNameDataGridViewTextBoxColumn1.Width = 139;
            // 
            // cImageDataGridViewTextBoxColumn1
            // 
            this.cImageDataGridViewTextBoxColumn1.DataPropertyName = "C_Image";
            this.cImageDataGridViewTextBoxColumn1.HeaderText = "File ảnh";
            this.cImageDataGridViewTextBoxColumn1.Name = "cImageDataGridViewTextBoxColumn1";
            // 
            // totalDataGridViewTextBoxColumn1
            // 
            this.totalDataGridViewTextBoxColumn1.DataPropertyName = "Total";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "N0";
            this.totalDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.totalDataGridViewTextBoxColumn1.HeaderText = "Giá";
            this.totalDataGridViewTextBoxColumn1.Name = "totalDataGridViewTextBoxColumn1";
            // 
            // ComboUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.comboInfo);
            this.Controls.Add(this.comboList);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ComboUserControl";
            this.Size = new System.Drawing.Size(1149, 839);
            this.Load += new System.EventHandler(this.ComboUserControl_Load);
            this.comboList.ResumeLayout(false);
            this.comboList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComboList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsCombo)).EndInit();
            this.comboInfo.ResumeLayout(false);
            this.comboInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgComboProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsComboProductModel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox comboList;
        private System.Windows.Forms.DataGridView dtgComboList;
        private System.Windows.Forms.BindingSource bdsCombo;
        private System.Windows.Forms.TextBox txtSearchCombo;
        private System.Windows.Forms.Label lblSearchCombo;
        private System.Windows.Forms.GroupBox comboInfo;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtComboImage;
        private System.Windows.Forms.Label lblComboImage;
        private System.Windows.Forms.TextBox txtComboName;
        private System.Windows.Forms.Label lblComboName;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnInsertProduct;
        private System.Windows.Forms.Button btnUpdateProductQuantity;
        private System.Windows.Forms.DataGridView dtgComboProduct;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.BindingSource bdsComboProductModel;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comboNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cImageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnClearImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comboIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitPriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn comboNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cImageDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn1;
    }
}
