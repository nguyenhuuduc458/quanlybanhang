﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Service;
using Repository.Models;
using System.Security.Cryptography;

namespace QuanLyBanHang_Admin.UserControls.ComboUserControl
{
    public partial class ComboUserControl : UserControl
    {
        #region CUSTOM VARIABLES
        OpenFileDialog openFileDialog = new OpenFileDialog();
        ComboService comboService = new ComboService();
        ComboProductService comboProductService = new ComboProductService();
        ProductService productService = new ProductService();
        public static Combo currentCombo=null;
        public static Product currentProduct = null;
        public static Combo_Product currentComboProduct = null;
        #endregion

        public ComboUserControl()
        {
            InitializeComponent();
        }

        public void loadData()
        {
            bdsCombo.DataSource = comboService.getAllCombo(null);
            if (currentCombo != null)
            {
                txtTotal.Text = comboProductService.getAllModelByComboId(currentCombo.Id).Sum(c => c.Amount).ToString("N0"); ;
            }
            else
            {
                txtTotal.Text = "";
            }
        }
        private void ComboUserControl_Load(object sender, EventArgs e)
        {
            loadData();
        }

        #region COMBO
        private void dtgComboList_CurrentCellChanged(object sender, EventArgs e)
        {
            Combo combo = bdsCombo.Current as Combo;
            currentCombo = combo;
            if (combo != null)
            {
                txtComboName.Text = combo.ComboName;
                txtComboImage.Text = combo.C_Image;
                bdsComboProductModel.DataSource = comboProductService.getAllModelByComboId(combo.Id);
                //txtTotal.Text = comboProductService.getTotalAmountByComboId(currentCombo.Id).ToString("N0");
                txtTotal.Text = comboProductService.getAllModelByComboId(combo.Id).Sum(c => c.Amount).ToString("N0");
            }
            else
            {
                txtComboName.Text = "";
                txtComboImage.Text = "";
                txtTotal.Text = "";
                bdsComboProductModel.DataSource = null;
            }
        }

        private void txtSearchCombo_TextChanged(object sender, EventArgs e)
        {
            bdsCombo.DataSource = comboService.getAllCombo(txtSearchCombo.Text);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            Combo combo = new Combo();
            combo.ComboName = txtComboName.Text;
            combo.C_Image = txtComboImage.Text;
            combo.Total = 0;
            if (isValid())
            {
                comboService.Add(combo);
                MessageBox.Show("Thêm combo thành công!");
                loadData();
            }
            else if (comboService.getAllCombo(txtComboName.Text) == null)
            {
                MessageBox.Show("Tên combo đã bị trùng, vui lòng kiểm tra lại!");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                Combo combo = currentCombo;
                combo.ComboName = txtComboName.Text;
                combo.C_Image = txtComboImage.Text;
                comboService.Update(combo);
                loadData();
                MessageBox.Show("Sửa đổi thành công!");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtComboName.ResetText();
            txtComboImage.ResetText();
            txtComboName.Focus();
        }

        private void btnClearImage_Click(object sender, EventArgs e)
        {
            txtComboImage.ResetText();
        }
        #endregion

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // to where your opendialog box get starting location
            openFileDialog.InitialDirectory = "C://Desktop";

            // which type image format you want to upload in database
            openFileDialog.Filter = "Image Only(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";

            // FilterIndex property represents the index of the filter currently selected in the file dialog box.
            openFileDialog.FilterIndex = 1;

            try
            {
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDialog.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(openFileDialog.FileName);
                        string filename = System.IO.Path.GetFileName(openFileDialog.FileName);
                        txtComboImage.Text = filename;
                    }
                }
            }
            catch (Exception ex)
            {
                // it will give if file is already exits..
                MessageBox.Show(ex.Message);
            }
        }

        #region COMBO_PRODUCT
        private void dtgComboProduct_CurrentCellChanged(object sender, EventArgs e)
        {
            currentComboProduct = bdsComboProductModel.Current as Combo_Product;
            if (currentComboProduct != null)
            {
                currentProduct = currentComboProduct.Product;
            }
        }

        private void btnInsertProduct_Click(object sender, EventArgs e)
        {
            ComboDetailInsertForm form = new ComboDetailInsertForm();
            form.FormClosed += form_FormClosed;
            form.Show();
        }

        private void btnUpdateProductQuantity_Click(object sender, EventArgs e)
        {
            ComboDetailUpdateForm form = new ComboDetailUpdateForm();
            form.FormClosed += form_FormClosed;
            form.Show();
        }
        #endregion

        #region CUSTOM FUNCTIONS
        private bool isValid()
        {
            string comboName = txtComboName.Text;

            if (InputChecker.isEmpty(comboName))
            {
                MessageBox.Show("Vui lòng tên combo.");
                txtComboName.Focus();
                return false;
            }

            return true;
        }

        private void form_FormClosed(object sender, FormClosedEventArgs e)
        {
            loadData();
        }
        #endregion
    }
}
