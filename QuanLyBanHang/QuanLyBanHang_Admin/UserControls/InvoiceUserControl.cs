﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Service;
using Repository.ViewModels;
using Repository.Models;

namespace QuanLyBanHang_Admin.UserControls
{
    public partial class InvoiceUserControl : UserControl
    {
        public InvoiceUserControl()
        {
            InitializeComponent();
        }

        private InvoiceService invoiceService = new InvoiceService();
        private List<InvoiceModel> listInvoiceOrigin = new List<InvoiceModel>();
        private List<InvoiceModel> listInvoiceForSearch = new List<InvoiceModel>();

        private void InvoiceUserControl_Load(object sender, EventArgs e)
        {
            //load InvoiceModels
            load();

        }

        private void load()
        {
            listInvoiceOrigin = invoiceService.getInvoiceModels();

            bdsInvoice.DataSource = listInvoiceOrigin;
            dtgInvoiceList.Refresh();

            if (listInvoiceOrigin != null && listInvoiceOrigin.Count != 0)
            {
                int invoiceid = listInvoiceOrigin.FirstOrDefault().Id;
                if(invoiceid != 0)
                //load invoicedetail_product by invoiceid
                bdsInvoiceDetailModel.DataSource = invoiceService.getInvoiceDetailProductModelsByInvoiceId(invoiceid);
            }
        }

        #region INVOICE
        private void txtSearchInvoice_TextChanged(object sender, EventArgs e)
       {
            string search = txtSearchInvoice.Text;
            if(string.IsNullOrEmpty(search))
            {
                bdsInvoice.DataSource = listInvoiceOrigin;
            }
            else
            {
                listInvoiceForSearch = listInvoiceOrigin;
                bdsInvoice.DataSource = listInvoiceForSearch.Where(x=>x.FullName.ToUpper().Contains(search.ToUpper())).ToList();
                dtgInvoiceList.Refresh();
            }
        }

        private void btnChangeStatus_Click(object sender, EventArgs e)
        {
            if(bdsInvoice.Current != null)
            {
                InvoiceModel model = bdsInvoice.Current as InvoiceModel;
                Invoice invoice = invoiceService.GetInvoiceById(model.Id);
                invoice.C_Status = cbStatus.Text;
                invoiceService.InvoiceUpdate(invoice);

                load();
                MessageBox.Show("Trạng thái đơn hàng đã thay đổi thành công");
            }
        }
        #endregion

        #region INVOICE_DETAIL
        private void dtgInvoiceList_CurrentCellChanged(object sender, EventArgs e)
        {
            if (bdsInvoice.Current != null )
            {
                //load invoice detail
                int invoiceId = (bdsInvoice.Current as InvoiceModel).Id;
                bdsInvoiceDetailModel.DataSource = invoiceService.getInvoiceDetailProductModelsByInvoiceId(invoiceId);
                dtgInvoiceDetail.Refresh();

                //load txt status
                cbStatus.Text = invoiceService.GetInvoiceById(invoiceId).C_Status;
            }
            else
            {
                bdsInvoiceDetailModel.DataSource = null;
                dtgInvoiceDetail.Refresh();
            }
        }
        #endregion
    }
}
