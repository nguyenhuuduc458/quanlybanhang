﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuanLyBanHang_Admin
{
    public class InputChecker
    {
        private static readonly string NUMBER_PATTERN = @"^[0-9]+$";
        private static readonly Regex NUMBER_REGEX = new Regex(NUMBER_PATTERN, RegexOptions.Compiled);

        public static bool isEmpty(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return true;
            }
            return false;
        }

        // check if a string given is number
        public static bool isNumber(string text)
        {
            return NUMBER_REGEX.IsMatch(text);
        }
        
        // check if a string given is money
        public static bool isMoney(string text)
        {
            bool check;
            double value;
            try
            {
                value = Double.Parse(text, CultureInfo.GetCultureInfo("en-US"));

                // if it's less than zero
                if (value < 0)
                {
                    check = false;
                }
                else check = true;
            }
            catch (Exception exception)
            {
                check = false;
            }
            return check;
        }
    }
}
